#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

#define max(x,y) (((x) >= (y)) ? (x) : (y))
#define min(x,y) (((x) <= (y)) ? (x) : (y))

float get_intersection_area(float left, float top, float right, float bottom, float left_gt, float top_gt, float right_gt, float bottom_gt)
{
    float xA = max(left, left_gt);
    float yA = max(top, top_gt);
    float xB = min(right, right_gt);
    float yB = min(bottom, bottom_gt);
    return (xB - xA + 1) * (yB - yA + 1);
}

int boxes_intersect(float left, float top, float right, float bottom, float left_gt, float top_gt, float right_gt, float bottom_gt)
{
    if (left > right_gt) return 0; // d1 is right of d2
    if (left_gt > right) return 0; // d1 is left of d2
    if (bottom < top_gt) return 0; // d1 is above of d2
    if (top > bottom_gt) return 0; // d1 is below of d2
    return 1;
}

float calculate_area(float left, float top, float right, float bottom)
{
    return (right - left + 1) * (bottom - top + 1);
}

int main(int argc, char *argv[])
{
    DIR* FD;
    struct dirent* in_file;
    FILE    *entry_file;
    
    FILE    *output_file;
    
    int n_filtered = 0;
    
    /* Check if both directories are provided as command line arguments */
    if (argc != 4) 
    {
        fprintf(stderr, "Usage: %s <input_directory> <output_directory> <groundtruth_directory>\n", argv[0]);
        return 1;
    }
    
    /* Scanning the input directory */
    if (NULL == (FD = opendir(argv[1])))
    {
        fprintf(stderr, "Error: Failed to open input directory '%s' - %s\n", argv[1], strerror(errno));
        return 1;
    }
    
    while ((in_file = readdir(FD))) 
    {
        if (!strcmp (in_file->d_name, "."))
            continue;
        if (!strcmp (in_file->d_name, ".."))    
            continue;
        
        char *ext;
        ext = strrchr(in_file->d_name, '.');
        
        if (0 == strcmp(ext, ".txt"))
        {
            char str[256];
            strcpy(str, argv[1]);
            strcat(str, in_file->d_name);
            
            //printf("FILE: %s\n", str);
            
            entry_file = fopen(str, "r");
            if (entry_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file - %s | %s\n", strerror(errno), str);
                return 1;
            }
            
            char str_out[256];
            strcpy(str_out, argv[2]);
            strcat(str_out, in_file->d_name);
            
            //printf("FILE: %s\n", str_out);
            
            output_file = fopen(str_out, "w+");
            if (output_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file - %s | %s\n", strerror(errno), str);
                return 1;
            }
            
            int class;
            float confidence, left, top, right, bottom;
            
            while (fscanf(entry_file, "%d %f %f %f %f %f", &class, &confidence, &left, &top, &right, &bottom) != EOF)
            {
    //             printf("%s %f %f %f %f %f\n", class, confidence, left, top, right, bottom);
                if (class == 2)
                {
                    FILE    *entry_file_gt;
                    
                    char str_gt[256];
                    strcpy(str_gt, argv[3]);
                    strcat(str_gt, in_file->d_name);
                    
                    //printf("FILE GT: %s\n", str_gt);
                    
                    entry_file_gt = fopen(str_gt, "r");
                    if (entry_file_gt == NULL)
                    {
                        fprintf(stderr, "Error : Failed to open entry file - %s | %s\n", strerror(errno), str_gt);
                        return 1;
                    }
                    
                    char class_gt[256];
                    int occluded;
                    float truncated, alpha, left_gt, top_gt, right_gt, bottom_gt, height, width, length, x, y, z, rotation_y;
                    
                    float discard = 0;
                    
                    while (fscanf(entry_file_gt, "%s %f %d %f %f %f %f %f %f %f %f %f %f %f %f", class_gt, &truncated, &occluded, &alpha, &left_gt, &top_gt, &right_gt, &bottom_gt, &height, &width, &length, &x, &y, &z, &rotation_y) != EOF)
                    {
    //                     printf("%s %f %d %f %f %f %f %f %f %f %f %f %f %f %f\n", class_gt, truncated, occluded, alpha, left_gt, top_gt, right_gt, bottom_gt, height, width, length, x, y, z, rotation_y);
                        if (strcmp(class_gt, "DontCare") == 0)
                        {
    //                         printf("%s %f %d %f %f %f %f %f %f %f %f %f %f %f %f\n", class_gt, truncated, occluded, alpha, left_gt, top_gt, right_gt, bottom_gt, height, width, length, x, y, z, rotation_y);
                            if (boxes_intersect(left, top, right, bottom, left_gt, top_gt, right_gt, bottom_gt))
                            {
                                float intersection_area = get_intersection_area(left, top, right, bottom, left_gt, top_gt, right_gt, bottom_gt);
                                float area = calculate_area(left, top, right, bottom);
                                float intserection_over_area = intersection_area / area;
    //                             printf("%f\n", intserection_over_area);
                                if (intserection_over_area > 0.5) discard = 1;
                            }
                        }
                    }
                    fclose(entry_file_gt);
                    
                    if (!discard) fprintf(output_file, "%d %f %f %f %f %f\n", class, confidence, left, top, right, bottom);
                    else ++n_filtered;
                }
            }
            fclose(entry_file);
            fclose(output_file);
        }
    }
    closedir(FD);
    printf("Filtered: %d\n", n_filtered);
}
