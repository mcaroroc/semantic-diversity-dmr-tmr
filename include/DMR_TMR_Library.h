#ifdef __cplusplus
extern "C" {
#endif

extern int DMR_execution;
extern int TMR_execution;

extern int AVG_merging;
extern int MAX_merging;
extern int VOT_merging;

/*
 * FAULT INJECTION PARAMETERS
*/

extern int FAULT_INJECTION; // 0 = no fault injection, 1 = fault injection
extern int FAULTY_LAYER; // 0 = not faulty, 1 = faulty
extern int SIMULTANEOUS_FAULTS; // 0 = fault in 1 component, 1 = fault in all components
extern int FAULTY_COMPONENT; // 0 = Output_0, 1 = Output_1, 2 = Output_2
extern int FAULTY_MUL; // 0 = no fault in mul, 1 = fault in multiplication
extern int FAULTY_ADD; // 0 = no fault in add, 1 = fault in add

/*
 * IMAGE TRANSFORMATION PARAMETERS
*/

extern float augmentation_gamma;
extern int augmentation_kernel_size;
extern float augmentation_mean;
extern float augmentation_sigma;
extern float augmentation_power;
extern int augmentation_low;
extern int augmentation_up;
extern int augmentation_kernel_rows;
extern int augmentation_kernel_cols;
extern float** augmentation_kernel_array;
extern int augmentation_pixel_shift;
extern float augmentation_angle;
extern float percentage_drop;

/*
 * DATASET PARAMETERS
*/

extern int KITTI_dataset;

void custom_gemm_ongpu(int TA, int TB, int M, int N, int K, float ALPHA, float *A, int lda, float *B, float *B_2, float *B_3, int ldb, float BETA, float *C, float *C_2, float *C_3, int ldc);
void init_img_transformation_params(char *filename);

void init_cuda_rand_state();
#ifdef __cplusplus
}
#endif
