from glob import glob
import os
import json
import sys
import shutil

def read_txt_to_list(path):
    with open(path) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    return content

def voc_ap(rec, prec):
    """
    --- Official matlab code VOC2012---
    mrec=[0 ; rec ; 1];
    mpre=[0 ; prec ; 0];
    for i=numel(mpre)-1:-1:1
            mpre(i)=max(mpre(i),mpre(i+1));
    end
    i=find(mrec(2:end)~=mrec(1:end-1))+1;
    ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
    """
    rec.insert(0, 0.0) # insert 0.0 at begining of list
    rec.append(1.0) # insert 1.0 at end of list
    mrec = rec[:]
    prec.insert(0, 0.0) # insert 0.0 at begining of list
    prec.append(0.0) # insert 0.0 at end of list
    mpre = prec[:]
    """
     This part makes the precision monotonically decreasing
        (goes from the end to the beginning)
        matlab: for i=numel(mpre)-1:-1:1
                    mpre(i)=max(mpre(i),mpre(i+1));
    """
    # matlab indexes start in 1 but python in 0, so I have to do:
    #     range(start=(len(mpre) - 2), end=0, step=-1)
    # also the python function range excludes the end, resulting in:
    #     range(start=(len(mpre) - 2), end=-1, step=-1)
    for i in range(len(mpre)-2, -1, -1):
        mpre[i] = max(mpre[i], mpre[i+1])
    """
     This part creates a list of indexes where the recall changes
        matlab: i=find(mrec(2:end)~=mrec(1:end-1))+1;
    """
    i_list = []
    for i in range(1, len(mrec)):
        if mrec[i] != mrec[i-1]:
            i_list.append(i) # if it was matlab would be i + 1
    """
     The Average Precision (AP) is the area under the curve
        (numerical integration)
        matlab: ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
    """
    ap = 0.0
    for i in i_list:
        ap += ((mrec[i]-mrec[i-1])*mpre[i])
    return ap, mrec, mpre


def eval_map(gt_folder_path, pred_folder_path, temp_json_folder_path, output_files_path, eval_dataset):
    total_gts = 0
    total_tps = 0
    total_fps = 0

    """Process Gt"""
    ground_truth_files_list = glob(gt_folder_path + '/*.txt')
    assert len(ground_truth_files_list) > 0, 'no ground truth file'
    ground_truth_files_list.sort()
    # dictionary with counter per class
    gt_counter_per_class = {}
    counter_images_per_class = {}

    gt_files = []
    for txt_file in ground_truth_files_list:
            file_id = txt_file.split(".txt", 1)[0]
            file_id = os.path.basename(os.path.normpath(file_id))
            # check if there is a correspondent detection-results file
            temp_path = os.path.join(pred_folder_path, (file_id + ".txt"))
            #assert os.path.exists(temp_path), "Error. File not found: {}\n".format(temp_path)
            lines_list = read_txt_to_list(txt_file)
            # create ground-truth dictionary
            bounding_boxes = []
            is_difficult = False
            already_seen_classes = []
            for line in lines_list:
              class_name, left, top, right, bottom = line.split()
              # check if class is in the ignore list, if yes skip
              bbox = left + " " + top + " " + right + " " + bottom
              #print(class_name, left, top, right, bottom)
              if (eval_dataset == 'COCO'): # ONLY GET PERSON AND VEHICLE OBJECT CLASSES
                if (class_name == '0' or class_name == '2' or class_name == '3' or class_name == '5' or class_name == '7'):
                  if (class_name == '2' or class_name == '3' or class_name == '5' or class_name == '7'):
                    class_name = '2'
                  bounding_boxes.append({"class_name": class_name, "bbox": bbox, "used": False})
                  total_gts += 1
                  # count that object
                  if class_name in gt_counter_per_class:
                    gt_counter_per_class[class_name] += 1
                  else:
                    # if class didn't exist yet
                    gt_counter_per_class[class_name] = 1

                  if class_name not in already_seen_classes:
                    if class_name in counter_images_per_class:
                        counter_images_per_class[class_name] += 1
                    else:
                        # if class didn't exist yet
                        counter_images_per_class[class_name] = 1
                    already_seen_classes.append(class_name)
              elif (eval_dataset == 'KITTI'):
                if (class_name == '2' or class_name == '7'):
                  class_name = '2'
                  bounding_boxes.append({"class_name": class_name, "bbox": bbox, "used": False})
                  total_gts += 1
                  # count that object
                  if class_name in gt_counter_per_class:
                    gt_counter_per_class[class_name] += 1
                  else:
                    # if class didn't exist yet
                    gt_counter_per_class[class_name] = 1

                  if class_name not in already_seen_classes:
                    if class_name in counter_images_per_class:
                        counter_images_per_class[class_name] += 1
                    else:
                        # if class didn't exist yet
                        counter_images_per_class[class_name] = 1
                    already_seen_classes.append(class_name)
              else:
                  bounding_boxes.append({"class_name": class_name, "bbox": bbox, "used": False})
                  total_gts += 1
                  # count that object
                  if class_name in gt_counter_per_class:
                    gt_counter_per_class[class_name] += 1
                  else:
                    # if class didn't exist yet
                    gt_counter_per_class[class_name] = 1

                  if class_name not in already_seen_classes:
                    if class_name in counter_images_per_class:
                        counter_images_per_class[class_name] += 1
                    else:
                        # if class didn't exist yet
                        counter_images_per_class[class_name] = 1
                    already_seen_classes.append(class_name)

            # dump bounding_boxes into a ".json" file
            new_temp_file = os.path.join(temp_json_folder_path, file_id+"_ground_truth.json") #TEMP_FILES_PATH + "/" + file_id + "_ground_truth.json"
            gt_files.append(new_temp_file)
            with open(new_temp_file, 'w') as outfile:
                json.dump(bounding_boxes, outfile)

    gt_classes = list(gt_counter_per_class.keys())
    # let's sort the classes alphabetically
    gt_classes = sorted(gt_classes)
    n_classes = len(gt_classes)
    #print(gt_classes, gt_counter_per_class)

    """Process prediction"""

    dr_files_list = sorted(glob(os.path.join(pred_folder_path, '*.txt')))
    #print(dr_files_list)

    for class_index, class_name in enumerate(gt_classes):
        bounding_boxes = []
        for txt_file in dr_files_list:
                # the first time it checks if all the corresponding ground-truth files exist
                file_id = txt_file.split(".txt", 1)[0]
                file_id = os.path.basename(os.path.normpath(file_id))
                temp_path = os.path.join(gt_folder_path, (file_id + ".txt"))
                if class_index == 0:
                    if not os.path.exists(temp_path):
                        error_msg = f"Error. File not found: {temp_path}\n"
                        print(error_msg)
                lines = read_txt_to_list(txt_file)
                for line in lines:
                    try:
                        tmp_class_name, confidence, left, top, right, bottom = line.split()
                        if (eval_dataset == 'COCO'):
                          if (tmp_class_name == '2' or tmp_class_name == '3' or tmp_class_name == '5' or tmp_class_name == '7'):
                            tmp_class_name = '2'
                        elif (eval_dataset == 'KITTI'):
                          if (tmp_class_name == '2' or tmp_class_name == '7'):
                            tmp_class_name = '2'
                    except ValueError:
                        error_msg = f"""Error: File {txt_file} in the wrong format.\n
                                        Expected: <class_name> <confidence> <left> <top> <right> <bottom>\n
                                        Received: {line} \n"""
                        print(error_msg)
                    if tmp_class_name == class_name:
                        # print("match")
                        bbox = left + " " + top + " " + right + " " + bottom
                        bounding_boxes.append({"confidence": confidence, "file_id": file_id, "bbox": bbox})
        # sort detection-results by decreasing confidence
        bounding_boxes.sort(key=lambda x: float(x['confidence']), reverse=True)
        with open(temp_json_folder_path + "/" + class_name + "_dr.json", 'w') as outfile:
            json.dump(bounding_boxes, outfile)

    """
        Calculate the AP for each class
    """
    sum_AP = 0.0
    ap_dictionary = {}
    # open file to store the output
    with open(output_files_path + "output.txt", 'w') as output_file:
        #output_file.write("# AP and precision/recall per class\n")
        count_true_positives = {}
        for class_index, class_name in enumerate(gt_classes):
            count_true_positives[class_name] = 0
            """
                Load detection-results of that class
            """
            dr_file = temp_json_folder_path + "/" + class_name + "_dr.json"
            dr_data = json.load(open(dr_file))

            """
                Assign detection-results to ground-truth objects
            """
            nd = len(dr_data)
            tp = [0] * nd  # creates an array of zeros of size nd
            fp = [0] * nd
            for idx, detection in enumerate(dr_data):
                file_id = detection["file_id"]
                gt_file = temp_json_folder_path + "/" + file_id + "_ground_truth.json"
                ground_truth_data = json.load(open(gt_file))
                ovmax = -1
                gt_match = -1
                # load detected object bounding-box
                bb = [float(x) for x in detection["bbox"].split()]
                for obj in ground_truth_data:
                    # look for a class_name match
                    if obj["class_name"] == class_name:
                        bbgt = [float(x) for x in obj["bbox"].split()]
                        bi = [max(bb[0], bbgt[0]), max(bb[1], bbgt[1]), min(bb[2], bbgt[2]), min(bb[3], bbgt[3])]
                        iw = bi[2] - bi[0] + 1
                        ih = bi[3] - bi[1] + 1
                        if iw > 0 and ih > 0:
                            # compute overlap (IoU) = area of intersection / area of union
                            ua = (bb[2] - bb[0] + 1) * (bb[3] - bb[1] + 1) + \
                                    (bbgt[2] - bbgt[0]+ 1) * (bbgt[3] - bbgt[1] + 1) - iw * ih
                            ov = iw * ih / ua
                            if ov > ovmax:
                                ovmax = ov
                                gt_match = obj

                min_overlap = 0.5
                if ovmax >= min_overlap:
                    # if "difficult" not in gt_match:
                    if not bool(gt_match["used"]):
                        # true positive
                        tp[idx] = 1
                        gt_match["used"] = True
                        count_true_positives[class_name] += 1
                        # update the ".json" file
                        with open(gt_file, 'w') as f:
                            f.write(json.dumps(ground_truth_data))
                    else:
                        # false positive (multiple detection)
                        fp[idx] = 1
                else:
                    fp[idx] = 1


            # compute precision/recall
            cumsum = 0
            for idx, val in enumerate(fp):
                fp[idx] += cumsum
                cumsum += val
            #print('fp ', cumsum)

            total_fps += cumsum

            cumsum = 0
            for idx, val in enumerate(tp):
                tp[idx] += cumsum
                cumsum += val
            #print('tp ', cumsum)

            total_tps += cumsum

            rec = tp[:]
            for idx, val in enumerate(tp):
                rec[idx] = float(tp[idx]) / gt_counter_per_class[class_name]
            #print('recall ', cumsum)
            prec = tp[:]
            for idx, val in enumerate(tp):
                prec[idx] = float(tp[idx]) / (fp[idx] + tp[idx])
            #print('prec ', cumsum)

            ap, mrec, mprec = voc_ap(rec[:], prec[:])
            sum_AP += ap
            text = "{0:.2f}%".format(
                ap * 100) + " = " + class_name + " AP "  # class_name + " AP = {0:.2f}%".format(ap*100)

            #print(text)
            ap_dictionary[class_name] = ap

            n_images = counter_images_per_class[class_name]

        # if show_animation:
        #     cv2.destroyAllWindows()

        #output_file.write("\n# mAP of all classes\n")
        if (n_classes == 0):
            mAP = 0
        else:
            mAP = sum_AP / n_classes
        #text = "mAP = {0:.2f}%".format(mAP * 100)
        #print(text)

        #print(total_gts, total_tps, total_fps)
        #print('total_gts')
        #print(total_gts)
        total_fns = total_gts - total_tps
        ACC = total_tps / (total_tps + total_fps + total_fns)
        print(total_tps, total_fps, total_fns, ACC, mAP)
        text = "TP: {0}, FP: {1}, FN: {2}, ACC: {3}, mAP: {4}".format(total_tps, total_fps, total_fns, ACC, mAP)
        output_file.write(text + "\n")
        
# Check if the correct number of arguments are provided
if len(sys.argv) != 6:
    print("Usage: python script.py gts_dir/ dets_dir/ json_dir/ output_dir/ eval_person_vehicle")
    sys.exit(1)

# Get command-line arguments
gts_dir = sys.argv[1]
dets_dir = sys.argv[2]
json_dir = sys.argv[3]
output_dir = sys.argv[4]
eval_dataset = sys.argv[5]

# Try to create json and output dirs
os.makedirs(json_dir, exist_ok=True)
os.makedirs(output_dir, exist_ok=True)

# Now you can use these variables as needed in your code
eval_map(gts_dir, dets_dir, json_dir, output_dir, eval_dataset)

# Remove json dir
shutil.rmtree(json_dir)
