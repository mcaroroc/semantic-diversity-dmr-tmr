#include <cuda_runtime.h>
#include <curand.h>
#include <cublas_v2.h>

#include "device_launch_parameters.h"

#include "DMR_TMR_Library.h"
#include "gemm.h"

#include <curand_kernel.h>

int DMR_execution = 0;
int TMR_execution = 0;

int AVG_merging = 0;
int MAX_merging = 0;
int VOT_merging = 0;

/*
* IMAGE TRANSFORMATION PARAMETERS
*/

float augmentation_gamma = 1.5;

int augmentation_kernel_size = 5;

float augmentation_mean = 0;
float augmentation_sigma = 10;

float augmentation_power = 0.9;

int augmentation_low = 0;
int augmentation_up = 25;

int augmentation_kernel_rows = 3;
int augmentation_kernel_cols = 3;

float** augmentation_kernel_array = NULL;

int augmentation_pixel_shift = 5;

float augmentation_angle = 0.8;

float percentage_drop = 0.05;

/*
* FAULT INJECTION
*/

curandState_t global_rand_state;
int FAULT_INJECTION = 0;
int FAULTY_LAYER = 0;
int SIMULTANEOUS_FAULTS = 0; // 0 = fault in 1 component, 1 = fault in all components
int FAULTY_COMPONENT = 0; // 0 = Output_0, 1 = Output_1, 2 = Output_2
int FAULTY_MUL = 0;
int FAULTY_ADD = 0;

/*
* DATASET PARAMETERS
*/

int KITTI_dataset = 0;

__global__ void init_cuda_rand_state_kernel(unsigned long *seed, curandState *state)
{
    if (threadIdx.x == 0 && blockIdx.x == 0)
    {
        printf("Initializing curand_init()\n");
        curand_init(*seed, 0, 0, state);
    }
}

void init_cuda_rand_state()
{
    unsigned long seed = time(NULL);

    unsigned long *d_seed;
    cudaMalloc(&d_seed, sizeof(unsigned long));
    cudaMemcpy(d_seed, &seed, sizeof(unsigned long), cudaMemcpyHostToDevice);

    curandState_t *d_state;
    cudaMalloc(&d_state, sizeof(curandState_t));
    cudaMemcpy(d_state, &global_rand_state, sizeof(curandState_t), cudaMemcpyHostToDevice);

    init_cuda_rand_state_kernel<<<1, 1>>>(d_seed, d_state);

    cudaMemcpy(&global_rand_state, d_state, sizeof(curandState_t), cudaMemcpyDeviceToHost);

    //printf("state->d = %u\n", global_rand_state.d);

    cudaFree(d_seed);
    cudaFree(d_state);
}

__device__ float fault_injection(float value, curandState_t *state)
{
    //printf("Injecting fault\n");
    // While loop done until no NaN or Infs are injected to avoid these errors
    while(1)
    {
        // Union to allow accessing the individual bits of a float
        union {
            float f;
            uint32_t i;
        } converter;

        converter.f = value;

        //printf("ori: %u\n", converter.i);

        //unsigned long seed = clock64() * (threadIdx.x + 1) * (blockIdx.x + 1);
        //curandState_t state;
        //curand_init(seed, 0, 0, &state);

        //printf("seed: %lu\n", seed);

        // Generate a random number between min and max
        //int bit_to_flip = curand(&state) % (32 - 24 + 1) + 24;

        int bit_to_flip = curand(state) % (32 - 24 + 1) + 24;
        //printf("bit_to_flip: %d\n", bit_to_flip);

        converter.i ^= static_cast<uint32_t>(1) << (bit_to_flip - 1);

        // Check if the resulting value is NaN or Inf
        if (!isnan(converter.f) && !isinf(converter.f))
        {
            //printf("FI Value: %f, %u\n", converter.f, converter.i);
            return converter.f;
        }
    }
    return value;
}

__global__ void gemm_FI(int M, int N, int K,
                        float* A, int lda,
                        float* B, int ldb,
                        float* C, int ldc,
                        int* inject_mul, int* inject_add, unsigned int* C_col_idx_FI, unsigned int *C_row_idx_FI, unsigned int *k_idx_FI, curandState_t *state)
{
    // Compute the row and column of C that this thread is responsible for.
    unsigned int const C_col_idx{blockIdx.x * blockDim.x + threadIdx.x};
    unsigned int const C_row_idx{blockIdx.y * blockDim.y + threadIdx.y};

    if (C_row_idx < M && C_col_idx < N)
    {
        float sum = 0;
        float mul_result = 0;

        for (int k_idx{0U}; k_idx < K; ++k_idx)
        {
            mul_result = A[C_row_idx * lda + k_idx] * B[k_idx * ldb + C_col_idx];

            if (*inject_mul && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("mul_result: %f, idx_a: %d, idx_b: %d\n", mul_result, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                mul_result = fault_injection(mul_result, state);
            }

            sum += mul_result;

            if (*inject_add && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("sum: %f, idx_a: %d, idx_b: %d\n", sum, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                sum = fault_injection(sum, state);
            }
        }
        C[C_row_idx * ldc + C_col_idx] = sum;
    }
}

__global__ void gemm_FI_DMR(int M, int N, int K,
                        float* A, int lda,
                        float* B, float * B_2, int ldb,
                        float* C, float* C_2, int ldc,
                        int *faulty_comp, int* inject_mul, int* inject_add, unsigned int* C_col_idx_FI, unsigned int *C_row_idx_FI, unsigned int *k_idx_FI, curandState_t *state)
{
    // Compute the row and column of C that this thread is responsible for.
    unsigned int const C_col_idx{blockIdx.x * blockDim.x + threadIdx.x};
    unsigned int const C_row_idx{blockIdx.y * blockDim.y + threadIdx.y};

    if (C_row_idx < M && C_col_idx < N)
    {
        float sum = 0;
        float mul_result = 0;

        float sum_2 = 0;
        float mul_result_2 = 0;

        for (int k_idx{0U}; k_idx < K; ++k_idx)
        {
            float weight = A[C_row_idx * lda + k_idx];
            mul_result = weight * B[k_idx * ldb + C_col_idx];
            mul_result_2 = weight * B_2[k_idx * ldb + C_col_idx];

            if (*inject_mul && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("mul_result: %f, mul_result_2: %f, idx_a: %d, idx_b: %d\n", mul_result, mul_result_2, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                if (faulty_comp[0]) mul_result = fault_injection(mul_result, state);
                if (faulty_comp[1]) mul_result_2 = fault_injection(mul_result_2, state);
            }

            sum += mul_result;
            sum_2 += mul_result_2;

            if (*inject_add && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("sum: %f, sum_2: %f, idx_a: %d, idx_b: %d\n", sum, sum_2, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                if (faulty_comp[0]) sum = fault_injection(sum, state);
                if (faulty_comp[1]) sum_2 = fault_injection(sum_2, state);
            }
        }
        C[C_row_idx * ldc + C_col_idx] = sum;
        C_2[C_row_idx * ldc + C_col_idx] = sum_2;
    }
}

__global__ void gemm_FI_TMR(int M, int N, int K,
                            float* A, int lda,
                            float* B, float* B_2, float* B_3, int ldb,
                            float* C, float* C_2, float* C_3, int ldc,
                            int *faulty_comp, int* inject_mul, int* inject_add, unsigned int* C_col_idx_FI, unsigned int *C_row_idx_FI, unsigned int *k_idx_FI, curandState_t *state)
{
    // Compute the row and column of C that this thread is responsible for.
    unsigned int const C_col_idx{blockIdx.x * blockDim.x + threadIdx.x};
    unsigned int const C_row_idx{blockIdx.y * blockDim.y + threadIdx.y};

    if (C_row_idx < M && C_col_idx < N)
    {
        float sum = 0;
        float mul_result = 0;

        float sum_2 = 0;
        float mul_result_2 = 0;

        float sum_3 = 0;
        float mul_result_3 = 0;

        for (int k_idx{0U}; k_idx < K; ++k_idx)
        {
            float weight = A[C_row_idx * lda + k_idx];
            mul_result = weight * B[k_idx * ldb + C_col_idx];
            mul_result_2 = weight * B_2[k_idx * ldb + C_col_idx];
            mul_result_3 = weight * B_3[k_idx * ldb + C_col_idx];

            if (*inject_mul && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("mul_result: %f, mul_result_2: %f, mul_result_3: %f, idx_a: %d, idx_b: %d\n", mul_result, mul_result_2, mul_result_3, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                if (faulty_comp[0]) mul_result = fault_injection(mul_result, state);
                if (faulty_comp[1]) mul_result_2 = fault_injection(mul_result_2, state);
                if (faulty_comp[2]) mul_result_3 = fault_injection(mul_result_3, state);
            }

            sum += mul_result;
            sum_2 += mul_result_2;
            sum_3 += mul_result_3;

            if (*inject_add && C_row_idx == *C_row_idx_FI && C_col_idx == *C_col_idx_FI && k_idx == *k_idx_FI)
            {
                //printf("sum: %f, sum_2: %f, sum_3: %f, idx_a: %d, idx_b: %d\n", sum, sum_2, sum_3, C_row_idx * lda + k_idx, k_idx * ldb + C_col_idx);
                if (faulty_comp[0]) sum = fault_injection(sum, state);
                if (faulty_comp[1]) sum_2 = fault_injection(sum_2, state);
                if (faulty_comp[2]) sum_3 = fault_injection(sum_3, state);
            }
        }
        C[C_row_idx * ldc + C_col_idx] = sum;
        C_2[C_row_idx * ldc + C_col_idx] = sum_2;
        C_3[C_row_idx * ldc + C_col_idx] = sum_3;
    }
}

void launch_gemm_FI(int M, int N, int K,
                    float * A, int lda, float* B, int ldb,
                    float* C, int ldc)
{
    //printf("GEMM FI\n");
    dim3 const block_dim{32U, 32U, 1U};
    dim3 const grid_dim{
        (static_cast<unsigned int>(N) + block_dim.x - 1U) / block_dim.x,
        (static_cast<unsigned int>(M) + block_dim.y - 1U) / block_dim.y, 1U};

    unsigned int C_col_idx_FI = rand() % N;
    unsigned int C_row_idx_FI = rand() % M;
    unsigned int k_idx_FI = rand() % K;

    // Copy fault injection flags to device memory
    int *d_inject_mul, *d_inject_add;
    cudaMalloc(&d_inject_mul, sizeof(int));
    cudaMalloc(&d_inject_add, sizeof(int));

    cudaMemcpy(d_inject_mul, &FAULTY_MUL, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_inject_add, &FAULTY_ADD, sizeof(int), cudaMemcpyHostToDevice);

    unsigned int *d_col_idx, *d_row_idx, *d_k_idx;
    cudaMalloc(&d_col_idx, sizeof(unsigned int));
    cudaMalloc(&d_row_idx, sizeof(unsigned int));
    cudaMalloc(&d_k_idx, sizeof(unsigned int));

    cudaMemcpy(d_col_idx, &C_col_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_row_idx, &C_row_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_k_idx, &k_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);

    curandState_t *d_state;
    cudaMalloc(&d_state, sizeof(curandState_t));
    cudaMemcpy(d_state, &global_rand_state, sizeof(curandState_t), cudaMemcpyHostToDevice);

    gemm_FI<<<grid_dim, block_dim>>>(M, N, K, A, lda, B, ldb, C, ldc, d_inject_mul, d_inject_add, d_col_idx, d_row_idx, d_k_idx, d_state);

    cudaMemcpy(&global_rand_state, d_state, sizeof(curandState_t), cudaMemcpyDeviceToHost);

    //printf("new state->d = %u\n", global_rand_state.d);

    cudaFree(d_col_idx);
    cudaFree(d_row_idx);
    cudaFree(d_k_idx);

    cudaFree(d_inject_mul);
    cudaFree(d_inject_add);

    cudaFree(d_state);
}

void launch_gemm_FI_DMR(int M, int N, int K,
                        float * A, int lda,
                        float* B, float* B_2, int ldb,
                        float* C, float* C_2, int ldc)
{
    //printf("GEMM FI\n");
    dim3 const block_dim{32U, 32U, 1U};
    dim3 const grid_dim{
        (static_cast<unsigned int>(N) + block_dim.x - 1U) / block_dim.x,
        (static_cast<unsigned int>(M) + block_dim.y - 1U) / block_dim.y, 1U};

    unsigned int C_col_idx_FI = rand() % N;
    unsigned int C_row_idx_FI = rand() % M;
    unsigned int k_idx_FI = rand() % K;

    // Copy fault injection flags to device memory
    int *d_inject_mul, *d_inject_add;
    cudaMalloc(&d_inject_mul, sizeof(int));
    cudaMalloc(&d_inject_add, sizeof(int));

    cudaMemcpy(d_inject_mul, &FAULTY_MUL, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_inject_add, &FAULTY_ADD, sizeof(int), cudaMemcpyHostToDevice);

    unsigned int *d_col_idx, *d_row_idx, *d_k_idx;
    cudaMalloc(&d_col_idx, sizeof(unsigned int));
    cudaMalloc(&d_row_idx, sizeof(unsigned int));
    cudaMalloc(&d_k_idx, sizeof(unsigned int));

    cudaMemcpy(d_col_idx, &C_col_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_row_idx, &C_row_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_k_idx, &k_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);

    curandState_t *d_state;
    cudaMalloc(&d_state, sizeof(curandState_t));
    cudaMemcpy(d_state, &global_rand_state, sizeof(curandState_t), cudaMemcpyHostToDevice);

    int faulty_comp[2];
    faulty_comp[0] = 0;
    faulty_comp[1] = 0;

    if (SIMULTANEOUS_FAULTS)
    {
        faulty_comp[0] = 1;
        faulty_comp[1] = 1;
    }
    else
    {
        if (FAULTY_COMPONENT == 0) faulty_comp[0] = 1;
        else if (FAULTY_COMPONENT == 1) faulty_comp[1] = 1;
    }

    int *d_faulty_comp;
    cudaMalloc(&d_faulty_comp, 2*sizeof(int));
    cudaMemcpy(d_faulty_comp, &faulty_comp, 2*sizeof(int), cudaMemcpyHostToDevice);

    gemm_FI_DMR<<<grid_dim, block_dim>>>(M, N, K, A, lda, B, B_2, ldb, C, C_2, ldc, d_faulty_comp, d_inject_mul, d_inject_add, d_col_idx, d_row_idx, d_k_idx, d_state);

    cudaMemcpy(&global_rand_state, d_state, sizeof(curandState_t), cudaMemcpyDeviceToHost);

    //printf("new state->d = %u\n", global_rand_state.d);

    cudaFree(d_col_idx);
    cudaFree(d_row_idx);
    cudaFree(d_k_idx);

    cudaFree(d_inject_mul);
    cudaFree(d_inject_add);

    cudaFree(d_faulty_comp);

    cudaFree(d_state);
}

void launch_gemm_FI_TMR(int M, int N, int K,
                        float* A, int lda,
                        float* B, float* B_2, float* B_3, int ldb,
                        float* C, float* C_2, float* C_3, int ldc)
{
    //printf("GEMM FI\n");
    dim3 const block_dim{32U, 32U, 1U};
    dim3 const grid_dim{
        (static_cast<unsigned int>(N) + block_dim.x - 1U) / block_dim.x,
        (static_cast<unsigned int>(M) + block_dim.y - 1U) / block_dim.y, 1U};

    unsigned int C_col_idx_FI = rand() % N;
    unsigned int C_row_idx_FI = rand() % M;
    unsigned int k_idx_FI = rand() % K;

    // Copy fault injection flags to device memory
    int *d_inject_mul, *d_inject_add;
    cudaMalloc(&d_inject_mul, sizeof(int));
    cudaMalloc(&d_inject_add, sizeof(int));

    cudaMemcpy(d_inject_mul, &FAULTY_MUL, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_inject_add, &FAULTY_ADD, sizeof(int), cudaMemcpyHostToDevice);

    unsigned int *d_col_idx, *d_row_idx, *d_k_idx;
    cudaMalloc(&d_col_idx, sizeof(unsigned int));
    cudaMalloc(&d_row_idx, sizeof(unsigned int));
    cudaMalloc(&d_k_idx, sizeof(unsigned int));

    cudaMemcpy(d_col_idx, &C_col_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_row_idx, &C_row_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_k_idx, &k_idx_FI, sizeof(unsigned int), cudaMemcpyHostToDevice);

    curandState_t *d_state;
    cudaMalloc(&d_state, sizeof(curandState_t));
    cudaMemcpy(d_state, &global_rand_state, sizeof(curandState_t), cudaMemcpyHostToDevice);

    int faulty_comp[3];
    faulty_comp[0] = 0;
    faulty_comp[1] = 0;
    faulty_comp[2] = 0;

    if (SIMULTANEOUS_FAULTS)
    {
        faulty_comp[0] = 1;
        faulty_comp[1] = 1;
        faulty_comp[2] = 1;
    }
    else
    {
        if (FAULTY_COMPONENT == 0) faulty_comp[0] = 1;
        else if (FAULTY_COMPONENT == 1) faulty_comp[1] = 1;
        else if (FAULTY_COMPONENT == 2) faulty_comp[2] = 1;
    }

    int *d_faulty_comp;
    cudaMalloc(&d_faulty_comp, 3*sizeof(int));
    cudaMemcpy(d_faulty_comp, &faulty_comp, 3*sizeof(int), cudaMemcpyHostToDevice);

    gemm_FI_TMR<<<grid_dim, block_dim>>>(M, N, K, A, lda, B, B_2, B_3, ldb, C, C_2, C_3, ldc, d_faulty_comp, d_inject_mul, d_inject_add, d_col_idx, d_row_idx, d_k_idx, d_state);

    cudaMemcpy(&global_rand_state, d_state, sizeof(curandState_t), cudaMemcpyDeviceToHost);

    //printf("new state->d = %u\n", global_rand_state.d);

    cudaFree(d_col_idx);
    cudaFree(d_row_idx);
    cudaFree(d_k_idx);

    cudaFree(d_inject_mul);
    cudaFree(d_inject_add);

    cudaFree(d_faulty_comp);

    cudaFree(d_state);
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t NUM_THREADS, size_t BLOCK_TILE_SKEW_SIZE_X = 0U, size_t BLOCK_TILE_SKEW_SIZE_Y = 0U>
__device__ void load_data_to_shared_memory_transposed(T const* A, size_t lda,
                                                    T const* B, size_t ldb,
                                                    T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y + BLOCK_TILE_SKEW_SIZE_Y],
                                                    T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                    size_t thread_block_tile_idx,
                                                    size_t thread_linear_idx,
                                                    size_t m, size_t n,
                                                    size_t k)
{
// Load data from A on DRAM to A_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_Y * BLOCK_TILE_SIZE_K + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const A_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_K};
        size_t const A_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_K};
        size_t const A_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + A_thread_block_tile_row_idx};
        size_t const A_col_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + A_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        if (A_row_idx < m && A_col_idx < k)
        {
            val = A[A_row_idx * lda + A_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{A[A_row_idx * lda + A_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_Y % NUM_THREADS == 0U);
        // if (A_thread_block_tile_row_idx < BLOCK_TILE_SIZE_Y &&
        //     A_thread_block_tile_col_idx < BLOCK_TILE_SIZE_K)
        // {
        //     A_thread_block_tile[A_thread_block_tile_row_idx]
        //                        [A_thread_block_tile_col_idx] = val;
        // }
        A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx] = val;
    }
// Load data from B on DRAM to B_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_X + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const B_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_X};
        size_t const B_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_X};
        size_t const B_row_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + B_thread_block_tile_row_idx};
        size_t const B_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + B_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        if (B_row_idx < k && B_col_idx < n)
        {
            val = B[B_row_idx * ldb + B_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{B[B_row_idx * ldb + B_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_X * BLOCK_TILE_SIZE_K % NUM_THREADS == 0U);
        // if (B_thread_block_tile_row_idx < BLOCK_TILE_SIZE_K &&
        //     B_thread_block_tile_col_idx < BLOCK_TILE_SIZE_X)
        // {
        //     B_thread_block_tile[B_thread_block_tile_row_idx]
        //                        [B_thread_block_tile_col_idx] = val;
        // }
        B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val;
    }
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t WARP_TILE_SIZE_X,
        size_t WARP_TILE_SIZE_Y, size_t THREAD_TILE_SIZE_X,
        size_t THREAD_TILE_SIZE_Y, size_t NUM_THREADS_PER_WARP_X,
        size_t NUM_THREADS_PER_WARP_Y>
__global__ void custom_gemm_optimized(size_t m, size_t n, size_t k, T const* A,
                                    size_t lda, T const* B, size_t ldb, T* C,
                                    size_t ldc)
{
    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    constexpr size_t NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    constexpr size_t NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_X{WARP_TILE_SIZE_X / (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X)};
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_Y{WARP_TILE_SIZE_Y / (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y)};
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};
    // Avoid using blockDim.x * blockDim.y as the number of threads per block.
    // Because it is a runtime constant and the compiler cannot optimize the
    // loop unrolling based on that.
    // Use a compile time constant instead.
    constexpr size_t NUM_THREADS{NUM_THREADS_X * NUM_THREADS_Y};

    // Cache a tile of A and B in shared memory for data reuse.
    __shared__ T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y];
    __shared__ T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];

    // A_vals is cached in the register.
    T A_vals[NUM_THREAD_TILES_PER_WARP_Y][THREAD_TILE_SIZE_Y] = {static_cast<T>(0)};
    // B_vals is cached in the register.
    T B_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    size_t const thread_linear_idx{threadIdx.y * blockDim.x + threadIdx.x};
    size_t const warp_linear_idx{thread_linear_idx / 32U};
    size_t const warp_row_idx{warp_linear_idx / NUM_WARPS_X};
    size_t const warp_col_idx{warp_linear_idx % NUM_WARPS_X};
    size_t const thread_linear_idx_in_warp{thread_linear_idx % 32U};
    size_t const thread_linear_row_idx_in_warp{thread_linear_idx_in_warp / NUM_THREADS_PER_WARP_X};
    size_t const thread_linear_col_idx_in_warp{thread_linear_idx_in_warp % NUM_THREADS_PER_WARP_X};

    // Number of outer loops to perform the sum of inner products.
    // C_thread_block_tile =
    // \sigma_{thread_block_tile_idx=0}^{num_thread_block_tiles-1} A[:,
    // thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
    // B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :]
    size_t const num_thread_block_tiles{(k + BLOCK_TILE_SIZE_K - 1) / BLOCK_TILE_SIZE_K};
    // Each thread in the block processes NUM_THREAD_TILES_PER_WARP_Y *
    // NUM_THREAD_TILES_PER_WARP_X * THREAD_TILE_SIZE_Y *
    // THREAD_TILE_SIZE_X output values.
    T C_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    for (size_t thread_block_tile_idx{0U}; thread_block_tile_idx < num_thread_block_tiles; ++thread_block_tile_idx)
    {
        load_data_to_shared_memory_transposed<T, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, NUM_THREADS>(A, lda, B, ldb, A_thread_block_tile_transposed, B_thread_block_tile, thread_block_tile_idx, thread_linear_idx, m, n, k);
        __syncthreads();

// Perform A[:, thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] where A[:,
// thread_block_tile_idx:BLOCK_TILE_SIZE_K] and
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] are cached in the
// shared memory as A_thread_block_tile and B_thread_block_tile,
// respectively. This inner product is further decomposed to
// BLOCK_TILE_SIZE_K outer products. A_thread_block_tile *
// B_thread_block_tile = \sigma_{k_i=0}^{BLOCK_TILE_SIZE_K-1}
// A_thread_block_tile[:, k_i] @ B_thread_block_tile[k_i, :] Note that
// both A_thread_block_tile and B_thread_block_tile can be cached in the
// register.
#pragma unroll
        for (size_t k_i{0U}; k_i < BLOCK_TILE_SIZE_K; ++k_i)
        {
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
                size_t const A_thread_block_tile_row_idx{warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y};
                size_t const A_thread_block_tile_col_idx{k_i};
#pragma unroll
                for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                {
                    A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] = A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx + thread_tile_y_idx];
                }
            }
#pragma unroll
            for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
            {
                size_t const B_thread_block_tile_row_idx{k_i};
                size_t const B_thread_block_tile_col_idx{warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X};
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];
                }
            }

// Compute NUM_THREAD_TILES_PER_WARP_Y * NUM_THREAD_TILES_PER_WARP_X outer
// products.
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
#pragma unroll
                for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
                {
#pragma unroll
                    for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                    {
#pragma unroll
                        for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                        {
                            C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];
                        }
                    }
                }
            }
        }
        // We can use syncwarp now.
        __syncwarp();
    }
    // Need a synchronization before writing the results to DRAM.
    __syncthreads();

// Write the results to DRAM.
#pragma unroll
    for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
    {
#pragma unroll
        for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
        {
#pragma unroll
            for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
            {
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    size_t const C_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y + thread_tile_y_idx};
                    size_t const C_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X + thread_tile_x_idx};
                    if (C_row_idx < m && C_col_idx < n)
                    {
                        C[C_row_idx * ldc + C_col_idx] += C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];
                    }
                }
            }
        }
    }
}

void launch_custom_gemm_nn_ongpu(int M, int N, int K,
    float *A, int lda,
    float *B, int ldb,
    float *C, int ldc)
{
    // Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/blob/main/src/06_2d_block_tiling_2d_warp_tiling_2d_thread_tiling_matrix_transpose.cu
    constexpr unsigned int BLOCK_TILE_SIZE_X{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_Y{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_K{4U};

    constexpr unsigned int WARP_TILE_SIZE_X{64U};
    constexpr unsigned int WARP_TILE_SIZE_Y{16U};

    constexpr unsigned int NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    constexpr unsigned int NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);

    constexpr unsigned int THREAD_TILE_SIZE_X{4U};
    constexpr unsigned int THREAD_TILE_SIZE_Y{4U};

    constexpr unsigned int NUM_THREADS_PER_WARP_X{16U};
    constexpr unsigned int NUM_THREADS_PER_WARP_Y{2U};

    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};

    constexpr unsigned int NUM_THREADS_PER_BLOCK{NUM_THREADS_X * NUM_THREADS_Y};

    dim3 const block_dim{NUM_THREADS_PER_BLOCK, 1U, 1U};
    dim3 const grid_dim{(static_cast<unsigned int>(N) + BLOCK_TILE_SIZE_X - 1U) / BLOCK_TILE_SIZE_X, (static_cast<unsigned int>(M) + BLOCK_TILE_SIZE_Y - 1U) / BLOCK_TILE_SIZE_Y, 1U};

    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B, ldb, C, ldc);
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t NUM_THREADS, size_t BLOCK_TILE_SKEW_SIZE_X = 0U, size_t BLOCK_TILE_SKEW_SIZE_Y = 0U>
__device__ void load_data_to_shared_memory_transposed_fused_DMR(T const* A, size_t lda,
                                                                T const* B, T const* B_2, size_t ldb,
                                                                T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y + BLOCK_TILE_SKEW_SIZE_Y],
                                                                T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                                T B_2_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                                size_t thread_block_tile_idx,
                                                                size_t thread_linear_idx,
                                                                size_t m, size_t n,
                                                                size_t k)
{
// Load data from A on DRAM to A_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_Y * BLOCK_TILE_SIZE_K + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const A_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_K};
        size_t const A_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_K};
        size_t const A_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + A_thread_block_tile_row_idx};
        size_t const A_col_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + A_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        if (A_row_idx < m && A_col_idx < k)
        {
            val = A[A_row_idx * lda + A_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{A[A_row_idx * lda + A_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_Y % NUM_THREADS == 0U);
        // if (A_thread_block_tile_row_idx < BLOCK_TILE_SIZE_Y &&
        //     A_thread_block_tile_col_idx < BLOCK_TILE_SIZE_K)
        // {
        //     A_thread_block_tile[A_thread_block_tile_row_idx]
        //                        [A_thread_block_tile_col_idx] = val;
        // }
        A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx] = val;
    }
// Load data from B on DRAM to B_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_X + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const B_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_X};
        size_t const B_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_X};
        size_t const B_row_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + B_thread_block_tile_row_idx};
        size_t const B_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + B_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        T val_2{static_cast<T>(0)};
        if (B_row_idx < k && B_col_idx < n)
        {
            val = B[B_row_idx * ldb + B_col_idx];
            val_2 = B_2[B_row_idx * ldb + B_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{B[B_row_idx * ldb + B_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_X * BLOCK_TILE_SIZE_K % NUM_THREADS == 0U);
        // if (B_thread_block_tile_row_idx < BLOCK_TILE_SIZE_K &&
        //     B_thread_block_tile_col_idx < BLOCK_TILE_SIZE_X)
        // {
        //     B_thread_block_tile[B_thread_block_tile_row_idx]
        //                        [B_thread_block_tile_col_idx] = val;
        // }
        B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val;
        B_2_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val_2;
    }
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t WARP_TILE_SIZE_X,
        size_t WARP_TILE_SIZE_Y, size_t THREAD_TILE_SIZE_X,
        size_t THREAD_TILE_SIZE_Y, size_t NUM_THREADS_PER_WARP_X,
        size_t NUM_THREADS_PER_WARP_Y>
__global__ void custom_gemm_optimized_fused_DMR(size_t m, size_t n, size_t k,
                                                T const* A, size_t lda,
                                                T const* B, T const* B_2, size_t ldb,
                                                T* C, T* C_2, size_t ldc)
{
    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    constexpr size_t NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    constexpr size_t NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_X{WARP_TILE_SIZE_X / (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X)};
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_Y{WARP_TILE_SIZE_Y / (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y)};
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};
    // Avoid using blockDim.x * blockDim.y as the number of threads per block.
    // Because it is a runtime constant and the compiler cannot optimize the
    // loop unrolling based on that.
    // Use a compile time constant instead.
    constexpr size_t NUM_THREADS{NUM_THREADS_X * NUM_THREADS_Y};

    // Cache a tile of A and B in shared memory for data reuse.
    __shared__ T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y];
    __shared__ T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];
    __shared__ T B_2_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];

    // A_vals is cached in the register.
    T A_vals[NUM_THREAD_TILES_PER_WARP_Y][THREAD_TILE_SIZE_Y] = {static_cast<T>(0)};
    // B_vals is cached in the register.
    T B_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    // B_2_vals is cached in the register.
    T B_2_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    size_t const thread_linear_idx{threadIdx.y * blockDim.x + threadIdx.x};
    size_t const warp_linear_idx{thread_linear_idx / 32U};
    size_t const warp_row_idx{warp_linear_idx / NUM_WARPS_X};
    size_t const warp_col_idx{warp_linear_idx % NUM_WARPS_X};
    size_t const thread_linear_idx_in_warp{thread_linear_idx % 32U};
    size_t const thread_linear_row_idx_in_warp{thread_linear_idx_in_warp / NUM_THREADS_PER_WARP_X};
    size_t const thread_linear_col_idx_in_warp{thread_linear_idx_in_warp % NUM_THREADS_PER_WARP_X};

    // Number of outer loops to perform the sum of inner products.
    // C_thread_block_tile =
    // \sigma_{thread_block_tile_idx=0}^{num_thread_block_tiles-1} A[:,
    // thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
    // B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :]
    size_t const num_thread_block_tiles{(k + BLOCK_TILE_SIZE_K - 1) / BLOCK_TILE_SIZE_K};
    // Each thread in the block processes NUM_THREAD_TILES_PER_WARP_Y *
    // NUM_THREAD_TILES_PER_WARP_X * THREAD_TILE_SIZE_Y *
    // THREAD_TILE_SIZE_X output values.
    T C_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    T C_2_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    for (size_t thread_block_tile_idx{0U}; thread_block_tile_idx < num_thread_block_tiles; ++thread_block_tile_idx)
    {
        load_data_to_shared_memory_transposed_fused_DMR<T, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, NUM_THREADS>(A, lda, B, B_2, ldb, A_thread_block_tile_transposed, B_thread_block_tile, B_2_thread_block_tile, thread_block_tile_idx, thread_linear_idx, m, n, k);
        __syncthreads();

// Perform A[:, thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] where A[:,
// thread_block_tile_idx:BLOCK_TILE_SIZE_K] and
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] are cached in the
// shared memory as A_thread_block_tile and B_thread_block_tile,
// respectively. This inner product is further decomposed to
// BLOCK_TILE_SIZE_K outer products. A_thread_block_tile *
// B_thread_block_tile = \sigma_{k_i=0}^{BLOCK_TILE_SIZE_K-1}
// A_thread_block_tile[:, k_i] @ B_thread_block_tile[k_i, :] Note that
// both A_thread_block_tile and B_thread_block_tile can be cached in the
// register.
#pragma unroll
        for (size_t k_i{0U}; k_i < BLOCK_TILE_SIZE_K; ++k_i)
        {
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
                size_t const A_thread_block_tile_row_idx{warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y};
                size_t const A_thread_block_tile_col_idx{k_i};
#pragma unroll
                for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                {
                    A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] = A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx + thread_tile_y_idx];
                }
            }
#pragma unroll
            for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
            {
                size_t const B_thread_block_tile_row_idx{k_i};
                size_t const B_thread_block_tile_col_idx{warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X};
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];

                    B_2_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_2_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];
                }
            }

// Compute NUM_THREAD_TILES_PER_WARP_Y * NUM_THREAD_TILES_PER_WARP_X outer
// products.
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
#pragma unroll
                for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
                {
#pragma unroll
                    for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                    {
#pragma unroll
                        for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                        {
                            C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];

                            C_2_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_2_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];
                        }
                    }
                }
            }
        }
        // We can use syncwarp now.
        __syncwarp();
    }
    // Need a synchronization before writing the results to DRAM.
    __syncthreads();

// Write the results to DRAM.
#pragma unroll
    for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
    {
#pragma unroll
        for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
        {
#pragma unroll
            for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
            {
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    size_t const C_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y + thread_tile_y_idx};
                    size_t const C_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X + thread_tile_x_idx};
                    if (C_row_idx < m && C_col_idx < n)
                    {
                        C[C_row_idx * ldc + C_col_idx] += C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];

                        C_2[C_row_idx * ldc + C_col_idx] += C_2_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];
                    }
                }
            }
        }
    }
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
void launch_custom_gemm_nn_ongpu_DMR(int M, int N, int K,
    float *A, int lda,
    float *B, float *B_2, int ldb,
    float *C, float *C_2, int ldc)
{
    // Code extarcted from https://github.com/leimao/CUDA-GEMM-Optimization/blob/main/src/06_2d_block_tiling_2d_warp_tiling_2d_thread_tiling_matrix_transpose.cu
    constexpr unsigned int BLOCK_TILE_SIZE_X{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_Y{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_K{8U};

    constexpr unsigned int WARP_TILE_SIZE_X{64U};
    constexpr unsigned int WARP_TILE_SIZE_Y{8U};

    constexpr unsigned int NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    constexpr unsigned int NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);

    constexpr unsigned int THREAD_TILE_SIZE_X{4U};
    constexpr unsigned int THREAD_TILE_SIZE_Y{4U};

    constexpr unsigned int NUM_THREADS_PER_WARP_X{16U};
    constexpr unsigned int NUM_THREADS_PER_WARP_Y{2U};

    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};

    constexpr unsigned int NUM_THREADS_PER_BLOCK{NUM_THREADS_X * NUM_THREADS_Y};

    dim3 const block_dim{NUM_THREADS_PER_BLOCK, 1U, 1U};
    dim3 const grid_dim{(static_cast<unsigned int>(N) + BLOCK_TILE_SIZE_X - 1U) / BLOCK_TILE_SIZE_X, (static_cast<unsigned int>(M) + BLOCK_TILE_SIZE_Y - 1U) / BLOCK_TILE_SIZE_Y, 1U};

    custom_gemm_optimized_fused_DMR<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B, B_2, ldb, C, C_2, ldc);

    // Comment the previous line and Uncomment the next lines to run Parallel execution instead of Fused
    /*
    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B, ldb, C, ldc);

    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B_2, ldb, C_2, ldc);
    */
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t NUM_THREADS, size_t BLOCK_TILE_SKEW_SIZE_X = 0U, size_t BLOCK_TILE_SKEW_SIZE_Y = 0U>
__device__ void load_data_to_shared_memory_transposed_fused_TMR(T const* A, size_t lda,
                                                                T const* B, T const* B_2, T const* B_3, size_t ldb,
                                                                T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y + BLOCK_TILE_SKEW_SIZE_Y],
                                                                T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                                T B_2_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                                T B_3_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X + BLOCK_TILE_SKEW_SIZE_X],
                                                                size_t thread_block_tile_idx,
                                                                size_t thread_linear_idx,
                                                                size_t m, size_t n,
                                                                size_t k)
{
// Load data from A on DRAM to A_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_Y * BLOCK_TILE_SIZE_K + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const A_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_K};
        size_t const A_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_K};
        size_t const A_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + A_thread_block_tile_row_idx};
        size_t const A_col_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + A_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        if (A_row_idx < m && A_col_idx < k)
        {
            val = A[A_row_idx * lda + A_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{A[A_row_idx * lda + A_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_Y % NUM_THREADS == 0U);
        // if (A_thread_block_tile_row_idx < BLOCK_TILE_SIZE_Y &&
        //     A_thread_block_tile_col_idx < BLOCK_TILE_SIZE_K)
        // {
        //     A_thread_block_tile[A_thread_block_tile_row_idx]
        //                        [A_thread_block_tile_col_idx] = val;
        // }
        A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx] = val;
    }
// Load data from B on DRAM to B_thread_block_tile on shared memory.
#pragma unroll
    for (size_t load_idx{0U}; load_idx < (BLOCK_TILE_SIZE_K * BLOCK_TILE_SIZE_X + NUM_THREADS - 1U) / NUM_THREADS; ++load_idx)
    {
        size_t const B_thread_block_tile_row_idx{(thread_linear_idx + load_idx * NUM_THREADS) / BLOCK_TILE_SIZE_X};
        size_t const B_thread_block_tile_col_idx{(thread_linear_idx + load_idx * NUM_THREADS) % BLOCK_TILE_SIZE_X};
        size_t const B_row_idx{thread_block_tile_idx * BLOCK_TILE_SIZE_K + B_thread_block_tile_row_idx};
        size_t const B_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + B_thread_block_tile_col_idx};

        // These boundary checks might slow down the kernel to some extent.
        // But they guarantee the correctness of the kernel for all
        // different GEMM configurations.
        T val{static_cast<T>(0)};
        T val_2{static_cast<T>(0)};
        T val_3{static_cast<T>(0)};
        if (B_row_idx < k && B_col_idx < n)
        {
            val = B[B_row_idx * ldb + B_col_idx];
            val_2 = B_2[B_row_idx * ldb + B_col_idx];
            val_3 = B_3[B_row_idx * ldb + B_col_idx];
        }
        // Removing the if will give another ~2 FLOPs performance on RTX
        // 3090. But it will make the kernel incorrect for some GEMM
        // configurations. T val{B[B_row_idx * ldb + B_col_idx]}; This if
        // will slow down the kernel. Add static asserts from the host code
        // to guarantee this if is always true.
        static_assert(BLOCK_TILE_SIZE_X * BLOCK_TILE_SIZE_K % NUM_THREADS == 0U);
        // if (B_thread_block_tile_row_idx < BLOCK_TILE_SIZE_K &&
        //     B_thread_block_tile_col_idx < BLOCK_TILE_SIZE_X)
        // {
        //     B_thread_block_tile[B_thread_block_tile_row_idx]
        //                        [B_thread_block_tile_col_idx] = val;
        // }
        B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val;
        B_2_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val_2;
        B_3_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx] = val_3;
    }
}

// Code extracted from https://github.com/leimao/CUDA-GEMM-Optimization/
template <typename T, size_t BLOCK_TILE_SIZE_X, size_t BLOCK_TILE_SIZE_Y,
        size_t BLOCK_TILE_SIZE_K, size_t WARP_TILE_SIZE_X,
        size_t WARP_TILE_SIZE_Y, size_t THREAD_TILE_SIZE_X,
        size_t THREAD_TILE_SIZE_Y, size_t NUM_THREADS_PER_WARP_X,
        size_t NUM_THREADS_PER_WARP_Y>
__global__ void custom_gemm_optimized_fused_TMR(size_t m, size_t n, size_t k,
                                                T const* A, size_t lda,
                                                T const* B, T const* B_2, T const* B_3, size_t ldb,
                                                T* C, T* C_2, T* C_3, size_t ldc)
{
    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    constexpr size_t NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    constexpr size_t NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_X{WARP_TILE_SIZE_X / (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X)};
    constexpr unsigned int NUM_THREAD_TILES_PER_WARP_Y{WARP_TILE_SIZE_Y / (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y)};
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};
    // Avoid using blockDim.x * blockDim.y as the number of threads per block.
    // Because it is a runtime constant and the compiler cannot optimize the
    // loop unrolling based on that.
    // Use a compile time constant instead.
    constexpr size_t NUM_THREADS{NUM_THREADS_X * NUM_THREADS_Y};

    // Cache a tile of A and B in shared memory for data reuse.
    __shared__ T A_thread_block_tile_transposed[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_Y];
    __shared__ T B_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];
    __shared__ T B_2_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];
    __shared__ T B_3_thread_block_tile[BLOCK_TILE_SIZE_K][BLOCK_TILE_SIZE_X];

    // A_vals is cached in the register.
    T A_vals[NUM_THREAD_TILES_PER_WARP_Y][THREAD_TILE_SIZE_Y] = {static_cast<T>(0)};
    // B_vals is cached in the register.
    T B_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    // B_2_vals is cached in the register.
    T B_2_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    // B_3_vals is cached in the register.
    T B_3_vals[NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    size_t const thread_linear_idx{threadIdx.y * blockDim.x + threadIdx.x};
    size_t const warp_linear_idx{thread_linear_idx / 32U};
    size_t const warp_row_idx{warp_linear_idx / NUM_WARPS_X};
    size_t const warp_col_idx{warp_linear_idx % NUM_WARPS_X};
    size_t const thread_linear_idx_in_warp{thread_linear_idx % 32U};
    size_t const thread_linear_row_idx_in_warp{thread_linear_idx_in_warp / NUM_THREADS_PER_WARP_X};
    size_t const thread_linear_col_idx_in_warp{thread_linear_idx_in_warp % NUM_THREADS_PER_WARP_X};

    // Number of outer loops to perform the sum of inner products.
    // C_thread_block_tile =
    // \sigma_{thread_block_tile_idx=0}^{num_thread_block_tiles-1} A[:,
    // thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
    // B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :]
    size_t const num_thread_block_tiles{(k + BLOCK_TILE_SIZE_K - 1) / BLOCK_TILE_SIZE_K};
    // Each thread in the block processes NUM_THREAD_TILES_PER_WARP_Y *
    // NUM_THREAD_TILES_PER_WARP_X * THREAD_TILE_SIZE_Y *
    // THREAD_TILE_SIZE_X output values.
    T C_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    T C_2_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};
    T C_3_thread_results[NUM_THREAD_TILES_PER_WARP_Y][NUM_THREAD_TILES_PER_WARP_X][THREAD_TILE_SIZE_Y][THREAD_TILE_SIZE_X] = {static_cast<T>(0)};

    for (size_t thread_block_tile_idx{0U}; thread_block_tile_idx < num_thread_block_tiles; ++thread_block_tile_idx)
    {
        load_data_to_shared_memory_transposed_fused_TMR<T, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, NUM_THREADS>(A, lda, B, B_2, B_3, ldb, A_thread_block_tile_transposed, B_thread_block_tile, B_2_thread_block_tile, B_3_thread_block_tile, thread_block_tile_idx, thread_linear_idx, m, n, k);
        __syncthreads();

// Perform A[:, thread_block_tile_idx:BLOCK_TILE_SIZE_K] *
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] where A[:,
// thread_block_tile_idx:BLOCK_TILE_SIZE_K] and
// B[thread_block_tile_idx:BLOCK_TILE_SIZE_K, :] are cached in the
// shared memory as A_thread_block_tile and B_thread_block_tile,
// respectively. This inner product is further decomposed to
// BLOCK_TILE_SIZE_K outer products. A_thread_block_tile *
// B_thread_block_tile = \sigma_{k_i=0}^{BLOCK_TILE_SIZE_K-1}
// A_thread_block_tile[:, k_i] @ B_thread_block_tile[k_i, :] Note that
// both A_thread_block_tile and B_thread_block_tile can be cached in the
// register.
#pragma unroll
        for (size_t k_i{0U}; k_i < BLOCK_TILE_SIZE_K; ++k_i)
        {
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
                size_t const A_thread_block_tile_row_idx{warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y};
                size_t const A_thread_block_tile_col_idx{k_i};
#pragma unroll
                for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                {
                    A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] = A_thread_block_tile_transposed[A_thread_block_tile_col_idx][A_thread_block_tile_row_idx + thread_tile_y_idx];
                }
            }
#pragma unroll
            for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
            {
                size_t const B_thread_block_tile_row_idx{k_i};
                size_t const B_thread_block_tile_col_idx{warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X};
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];

                    B_2_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_2_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];

                    B_3_vals[thread_tile_repeat_col_idx][thread_tile_x_idx] = B_3_thread_block_tile[B_thread_block_tile_row_idx][B_thread_block_tile_col_idx + thread_tile_x_idx];
                }
            }

// Compute NUM_THREAD_TILES_PER_WARP_Y * NUM_THREAD_TILES_PER_WARP_X outer
// products.
#pragma unroll
            for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
            {
#pragma unroll
                for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
                {
#pragma unroll
                    for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
                    {
#pragma unroll
                        for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                        {
                            C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];

                            C_2_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_2_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];

                            C_3_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx] += A_vals[thread_tile_repeat_row_idx][thread_tile_y_idx] * B_3_vals[thread_tile_repeat_col_idx][thread_tile_x_idx];
                        }
                    }
                }
            }
        }
        // We can use syncwarp now.
        __syncwarp();
    }
    // Need a synchronization before writing the results to DRAM.
    __syncthreads();

// Write the results to DRAM.
#pragma unroll
    for (size_t thread_tile_repeat_row_idx{0U}; thread_tile_repeat_row_idx < NUM_THREAD_TILES_PER_WARP_Y; ++thread_tile_repeat_row_idx)
    {
#pragma unroll
        for (size_t thread_tile_repeat_col_idx{0U}; thread_tile_repeat_col_idx < NUM_THREAD_TILES_PER_WARP_X; ++thread_tile_repeat_col_idx)
        {
#pragma unroll
            for (size_t thread_tile_y_idx{0U}; thread_tile_y_idx < THREAD_TILE_SIZE_Y; ++thread_tile_y_idx)
            {
#pragma unroll
                for (size_t thread_tile_x_idx{0U}; thread_tile_x_idx < THREAD_TILE_SIZE_X; ++thread_tile_x_idx)
                {
                    size_t const C_row_idx{blockIdx.y * BLOCK_TILE_SIZE_Y + warp_row_idx * WARP_TILE_SIZE_Y + thread_tile_repeat_row_idx * (WARP_TILE_SIZE_Y / NUM_THREAD_TILES_PER_WARP_Y) + thread_linear_row_idx_in_warp * THREAD_TILE_SIZE_Y + thread_tile_y_idx};
                    size_t const C_col_idx{blockIdx.x * BLOCK_TILE_SIZE_X + warp_col_idx * WARP_TILE_SIZE_X + thread_tile_repeat_col_idx * (WARP_TILE_SIZE_X / NUM_THREAD_TILES_PER_WARP_X) + thread_linear_col_idx_in_warp * THREAD_TILE_SIZE_X + thread_tile_x_idx};
                    if (C_row_idx < m && C_col_idx < n)
                    {
                        C[C_row_idx * ldc + C_col_idx] += C_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];

                        C_2[C_row_idx * ldc + C_col_idx] += C_2_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];

                        C_3[C_row_idx * ldc + C_col_idx] += C_3_thread_results[thread_tile_repeat_row_idx][thread_tile_repeat_col_idx][thread_tile_y_idx][thread_tile_x_idx];
                    }
                }
            }
        }
    }
}

void launch_custom_gemm_nn_ongpu_TMR(int M, int N, int K,
    float *A, int lda,
    float *B, float *B_2, float *B_3, int ldb,
    float *C, float *C_2, float *C_3, int ldc)
{
    // Code extarcted from https://github.com/leimao/CUDA-GEMM-Optimization/blob/main/src/06_2d_block_tiling_2d_warp_tiling_2d_thread_tiling_matrix_transpose.cu
    constexpr unsigned int BLOCK_TILE_SIZE_X{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_Y{64U};
    constexpr unsigned int BLOCK_TILE_SIZE_K{16U};

    constexpr unsigned int WARP_TILE_SIZE_X{64U};
    constexpr unsigned int WARP_TILE_SIZE_Y{8U};

    constexpr unsigned int NUM_WARPS_X{BLOCK_TILE_SIZE_X / WARP_TILE_SIZE_X};
    constexpr unsigned int NUM_WARPS_Y{BLOCK_TILE_SIZE_Y / WARP_TILE_SIZE_Y};
    static_assert(BLOCK_TILE_SIZE_X % WARP_TILE_SIZE_X == 0U);
    static_assert(BLOCK_TILE_SIZE_Y % WARP_TILE_SIZE_Y == 0U);

    constexpr unsigned int THREAD_TILE_SIZE_X{4U};
    constexpr unsigned int THREAD_TILE_SIZE_Y{4U};

    constexpr unsigned int NUM_THREADS_PER_WARP_X{16U};
    constexpr unsigned int NUM_THREADS_PER_WARP_Y{2U};

    static_assert(NUM_THREADS_PER_WARP_X * NUM_THREADS_PER_WARP_Y == 32U);
    static_assert(WARP_TILE_SIZE_X % (THREAD_TILE_SIZE_X * NUM_THREADS_PER_WARP_X) == 0U);
    static_assert(WARP_TILE_SIZE_Y % (THREAD_TILE_SIZE_Y * NUM_THREADS_PER_WARP_Y) == 0U);

    constexpr unsigned int NUM_THREADS_X{NUM_WARPS_X * NUM_THREADS_PER_WARP_X};
    constexpr unsigned int NUM_THREADS_Y{NUM_WARPS_Y * NUM_THREADS_PER_WARP_Y};

    constexpr unsigned int NUM_THREADS_PER_BLOCK{NUM_THREADS_X * NUM_THREADS_Y};

    dim3 const block_dim{NUM_THREADS_PER_BLOCK, 1U, 1U};
    dim3 const grid_dim{(static_cast<unsigned int>(N) + BLOCK_TILE_SIZE_X - 1U) / BLOCK_TILE_SIZE_X, (static_cast<unsigned int>(M) + BLOCK_TILE_SIZE_Y - 1U) / BLOCK_TILE_SIZE_Y, 1U};

    custom_gemm_optimized_fused_TMR<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B, B_2, B_3, ldb, C, C_2, C_3, ldc);

    // Comment the previous line and Uncomment the next lines to run Parallel execution instead of Fused
    /*
    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B, ldb, C, ldc);

    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B_2, ldb, C_2, ldc);

    custom_gemm_optimized<float, BLOCK_TILE_SIZE_X, BLOCK_TILE_SIZE_Y, BLOCK_TILE_SIZE_K, WARP_TILE_SIZE_X, WARP_TILE_SIZE_Y, THREAD_TILE_SIZE_X, THREAD_TILE_SIZE_Y, NUM_THREADS_PER_WARP_X, NUM_THREADS_PER_WARP_Y> <<<grid_dim, block_dim, 0U, get_cuda_stream()>>>(M, N, K, A, lda, B_3, ldb, C_3, ldc);
    */
}

void custom_gemm_ongpu(int TA, int TB, int M, int N, int K, float ALPHA,
        float *A, int lda,
        float *B, float *B_2, float *B_3, int ldb,
        float BETA,
        float *C, float *C_2, float *C_3, int ldc)
{
if (!TA && !TB && ALPHA == 1 && BETA == 1)
{
    if (DMR_execution)
    {
        if (!FAULT_INJECTION || (FAULT_INJECTION && !FAULTY_LAYER)) launch_custom_gemm_nn_ongpu_DMR(M, N, K, A, lda, B, B_2, ldb, C, C_2, ldc);
        else launch_gemm_FI_DMR(M, N, K, A, lda, B, B_2, ldb, C, C_2, ldc);
    }
    else if (TMR_execution)
    {
        if (!FAULT_INJECTION || (FAULT_INJECTION && !FAULTY_LAYER)) launch_custom_gemm_nn_ongpu_TMR(M, N, K, A, lda, B, B_2, B_3, ldb, C, C_2, C_3, ldc);
        else launch_gemm_FI_TMR(M, N, K, A, lda, B, B_2, B_3, ldb, C, C_2, C_3, ldc);
    }
    else
    {
        if (!FAULT_INJECTION || (FAULT_INJECTION && !FAULTY_LAYER)) launch_custom_gemm_nn_ongpu(M, N, K, A, lda, B, ldb, C, ldc);
        else launch_gemm_FI(M, N, K, A, lda, B, ldb, C, ldc);
    }
}
else printf("This convolution is not implemented\n");
}
