#include "network.h"
#include "detection_layer.h"
#include "region_layer.h"
#include "cost_layer.h"
#include "utils.h"
#include "parser.h"
#include "box.h"
#include "image.h"
#include "demo.h"
#include "darknet.h"
#ifdef WIN32
#include <time.h>
#include "gettimeofday.h"
#else
#include <sys/time.h>
#endif

#include "parser.h"
#include "utils.h"
#include "dark_cuda.h"
#include "blas.h"
#include "connected_layer.h"

#include "DMR_TMR_Library.h"

#include <sys/stat.h>

#include "detector.h"

#ifdef OPENCV

#include "http_stream.h"

static char **demo_names;
static image **demo_alphabet;
static int demo_classes;

static int nboxes = 0;
static detection *dets = NULL;

static network net;
static image in_s ;
static image det_s;

static cap_cv *cap;
static float fps = 0;
static float demo_thresh = 0;
static int demo_ext_output = 0;
static long long int frame_id = 0;
static int demo_json_port = -1;
static bool demo_skip_frame = false;


static int avg_frames;
static int demo_index = 0;
static mat_cv** cv_images;

mat_cv* in_img;
mat_cv* det_img;
mat_cv* show_img;

static volatile int flag_exit;
static int letter_box = 0;

static const int thread_wait_ms = 1;
static volatile int run_fetch_in_thread = 0;
static volatile int run_detect_in_thread = 0;

// Code added for DMR, TMR
static int *img_transf = NULL;
static image in_s_2;
static image in_s_3;
static image det_s_2;
static image det_s_3;

static image in_img_original;

static int num_classes = 80;

static int nboxes_2 = 0;
static int nboxes_3 = 0;

static detection *dets_2 = NULL;
static detection *dets_3 = NULL;

void *fetch_in_thread(void *ptr)
{
    while (!custom_atomic_load_int(&flag_exit)) {
        while (!custom_atomic_load_int(&run_fetch_in_thread)) {
            if (custom_atomic_load_int(&flag_exit)) return 0;
            if (demo_skip_frame)
                consume_frame(cap);
            this_thread_yield();
        }
        int dont_close_stream = 0;    // set 1 if your IP-camera periodically turns off and turns on video-stream
        if (letter_box)
            in_s = get_image_from_stream_letterbox(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
        else
            in_s = get_image_from_stream_resize(cap, net.w, net.h, net.c, &in_img, dont_close_stream);
        if (!in_s.data) {
            printf("Stream closed.\n");
            custom_atomic_store_int(&flag_exit, 1);
            custom_atomic_store_int(&run_fetch_in_thread, 0);
            //exit(EXIT_FAILURE);
            return 0;
        }
        //in_s = resize_image(in, net.w, net.h);

        // CHANGES FOR DMR, TMR
        if (DMR_execution || TMR_execution)
        {
            in_s_2 = copy_image(in_s);

            if (TMR_execution)
            {
                in_s_3 = copy_image(in_s);
            }
        }

        in_s = image_data_augmentation_extended(in_s, img_transf[0]);

        if (DMR_execution || TMR_execution)
        {
            in_s_2 = image_data_augmentation_extended(in_s_2, img_transf[1]);

            if (TMR_execution)
            {
                in_s_3 = image_data_augmentation_extended(in_s_3, img_transf[2]);
            }
        }

        custom_atomic_store_int(&run_fetch_in_thread, 0);
    }
    return 0;
}

void *fetch_in_thread_sync(void *ptr)
{
    custom_atomic_store_int(&run_fetch_in_thread, 1);
    while (custom_atomic_load_int(&run_fetch_in_thread)) this_thread_sleep_for(thread_wait_ms);
    return 0;
}

void *detect_in_thread(void *ptr)
{
    //printf("Start detect_in_thread\n");
    while (!custom_atomic_load_int(&flag_exit)) {
        while (!custom_atomic_load_int(&run_detect_in_thread)) {
            if (custom_atomic_load_int(&flag_exit)) return 0;
            this_thread_yield();
        }

        layer l = net.layers[net.n - 1];
        float *X = det_s.data;
        float *X_2 = det_s_2.data;
        float *X_3 = det_s_3.data;

        //float *prediction =
        //network_predict(net, X);

        //printf("Start network_predict_custom\n");
        network_predict_custom(net, X, X_2, X_3);
        //printf("Done network_predict_custom\n");

        cv_images[demo_index] = det_img;
        det_img = cv_images[(demo_index + avg_frames / 2 + 1) % avg_frames];
        demo_index = (demo_index + 1) % avg_frames;

        if (letter_box)
        {
            printf("LETTER_BOX DMR/TMR SUPPORT NOT IMPLEMENTED\n");
            dets = get_network_boxes(&net, get_width_mat(in_img), get_height_mat(in_img), demo_thresh, demo_thresh, 0, 1, &nboxes, 1); // letter box
        }
        else
        {
            //printf("No Letter box\n");
            in_img_original = mat_to_image_cv(in_img);
            undo_transformation(&net, 1, img_transf[0], in_img_original);
            dets = get_network_boxes(&net, in_img_original.w, in_img_original.h, demo_thresh, demo_thresh, 0, 1, &nboxes, 0);
            calculate_best_class_idx(dets, nboxes);

            if (KITTI_dataset)
            {
                fuse_vehicle_classes_KITTI(dets, nboxes, num_classes);
                ignore_not_vehicle(dets, nboxes, num_classes);
            }
            else
            {
                fuse_vehicle_classes(dets, nboxes, num_classes);
                ignore_not_vehicle_person(dets, nboxes, num_classes);
            }

            if (FAULT_INJECTION) fix_zero_area_faults(dets, nboxes, num_classes);

            if (DMR_execution || TMR_execution)
            {
                undo_transformation(&net, 2, img_transf[1], in_img_original);
                dets_2 = get_network_boxes_2nd_input(&net, in_img_original.w, in_img_original.h, demo_thresh, demo_thresh, 0, 1, &nboxes_2, 0);
                calculate_best_class_idx(dets_2, nboxes_2);

                if (KITTI_dataset)
                {
                    fuse_vehicle_classes_KITTI(dets_2, nboxes_2, num_classes);
                    ignore_not_vehicle(dets_2, nboxes_2, num_classes);
                }
                else
                {
                    fuse_vehicle_classes(dets_2, nboxes_2, num_classes);
                    ignore_not_vehicle_person(dets_2, nboxes_2, num_classes);
                }

                if (FAULT_INJECTION) fix_zero_area_faults(dets_2, nboxes_2, num_classes);

                if (TMR_execution)
                {
                    undo_transformation(&net, 3, img_transf[2], in_img_original);
                    dets_3 = get_network_boxes_3rd_input(&net, in_img_original.w, in_img_original.h, demo_thresh, demo_thresh, 0, 1, &nboxes_3, 0);
                    calculate_best_class_idx(dets_3, nboxes_3);

                    if (KITTI_dataset)
                    {
                        fuse_vehicle_classes_KITTI(dets_3, nboxes_3, num_classes);
                        ignore_not_vehicle(dets_3, nboxes_3, num_classes);
                    }
                    else
                    {
                        fuse_vehicle_classes(dets_3, nboxes_3, num_classes);
                        ignore_not_vehicle_person(dets_3, nboxes_3, num_classes);
                    }

                    if (FAULT_INJECTION) fix_zero_area_faults(dets_3, nboxes_3, num_classes);
                }
            }
        }

        //const float nms = .45;
        //if (nms) {
        //    if (l.nms_kind == DEFAULT_NMS) do_nms_sort(dets, nboxes, l.classes, nms);
        //    else diounms_sort(dets, nboxes, l.classes, nms, l.nms_kind, l.beta_nms);
        //}

        //printf("Finished detect_in_thread\n");
        custom_atomic_store_int(&run_detect_in_thread, 0);
    }

    return 0;
}

void *detect_in_thread_sync(void *ptr)
{
    custom_atomic_store_int(&run_detect_in_thread, 1);
    while (custom_atomic_load_int(&run_detect_in_thread)) this_thread_sleep_for(thread_wait_ms);
    return 0;
}

double get_wall_time()
{
    struct timeval walltime;
    if (gettimeofday(&walltime, NULL)) {
        return 0;
    }
    return (double)walltime.tv_sec + (double)walltime.tv_usec * .000001;
}

void demo(char *cfgfile, char *weightfile, float thresh, float hier_thresh, int cam_index, const char *filename, char **names, int classes, int avgframes,
    int frame_skip, char *prefix, char *out_filename, int mjpeg_port, int dontdraw_bbox, int json_port, int dont_show, int ext_output, int letter_box_in, int time_limit_sec, char *http_post_host,
    int benchmark, int benchmark_layers, int *img_transformations, char *save_dets)
{
    // Code added for DMR, TMR
    printf("Starting Video Processing\n");

    // INitialize randomness
    srand(time(NULL));
    init_cuda_rand_state();

    int num_read_transformations;
    float iou_thresh = 0.5;

    if (DMR_execution) num_read_transformations = 2;
    else if (TMR_execution) num_read_transformations = 3;
    else num_read_transformations = 1;

    printf("DMR_execution: %d, TMR_execution: %d\n", DMR_execution, TMR_execution);
    printf("num_read_transformations: %d\n", num_read_transformations);

    img_transf = (int *)malloc(num_read_transformations * sizeof(int)); // Dynamically allocate memory

    if (DMR_execution)
    {
        img_transf[0] = img_transformations[0];
        img_transf[1] = img_transformations[1];

        printf("DMR transformations: %d, %d\n", img_transf[0], img_transf[1]);
    }
    else if (TMR_execution)
    {
        img_transf[0] = img_transformations[0];
        img_transf[1] = img_transformations[1];
        img_transf[2] = img_transformations[2];

        printf("TMR transformations: %d, %d, %d\n", img_transf[0], img_transf[1], img_transf[2]);
    }
    else
    {
        img_transf[0] = img_transformations[0];
        printf("Single Execution transformations: %d\n", img_transf[0]);
    }


    if (avgframes < 1) avgframes = 1;
    avg_frames = avgframes;
    letter_box = letter_box_in;
    in_img = det_img = show_img = NULL;
    //skip = frame_skip;
    image **alphabet = load_alphabet();
    int delay = frame_skip;
    demo_names = names;
    demo_alphabet = alphabet;
    demo_classes = classes;
    demo_thresh = thresh;
    demo_ext_output = ext_output;
    demo_json_port = json_port;
    printf("Demo\n");
    net = parse_network_cfg_custom(cfgfile, 1, 1);    // set batch=1
    if(weightfile){
        load_weights(&net, weightfile);
    }
    if (net.letter_box) letter_box = 1;
    net.benchmark_layers = benchmark_layers;
    fuse_conv_batchnorm(net);
    calculate_binary_weights(net);
    //srand(2222222);

    if(filename){
        printf("video file: %s\n", filename);
        cap = get_capture_video_stream(filename);
        demo_skip_frame = is_live_stream(filename);
    }else{
        printf("Webcam index: %d\n", cam_index);
        cap = get_capture_webcam(cam_index);
        demo_skip_frame = true;
    }

    if (!cap) {
#ifdef WIN32
        printf("Check that you have copied file opencv_ffmpeg340_64.dll to the same directory where is darknet.exe \n");
#endif
        error("Couldn't connect to webcam.", DARKNET_LOC);
    }

    layer l = net.layers[net.n-1];
    int j;

    cv_images = (mat_cv**)xcalloc(avg_frames, sizeof(mat_cv));

    printf("avg_frames: %d\n", avg_frames);

    int i;
    for (i = 0; i < net.n; ++i) {
        layer lc = net.layers[i];
        if (lc.type == YOLO) {
            lc.mean_alpha = 1.0 / avg_frames;
            l = lc;
        }
    }

    if (l.classes != demo_classes) {
        printf("\n Parameters don't match: in cfg-file classes=%d, in data-file classes=%d \n", l.classes, demo_classes);
        getchar();
        exit(0);
    }

    flag_exit = 0;

    //printf("custom_thread_create\n");
    custom_thread_t fetch_thread = NULL;
    custom_thread_t detect_thread = NULL;
    if (custom_create_thread(&fetch_thread, 0, fetch_in_thread, 0)) error("Thread creation failed", DARKNET_LOC);
    if (custom_create_thread(&detect_thread, 0, detect_in_thread, 0)) error("Thread creation failed", DARKNET_LOC);

    //printf("fetch_in_thread_sync\n");
    fetch_in_thread_sync(0); //fetch_in_thread(0);
    det_img = in_img;
    det_s = in_s;
    det_s_2 = in_s_2;
    det_s_3 = in_s_3;

    //printf("Save det_s.jpg\n");
    //save_cv_jpg(det_s, "det_s.jpg");
    //printf("Save det_s_2.jpg");
    //save_cv_jpg(det_s_2, "det_s_2.jpg");

    //printf("fetch_in_thread_sync\n");
    fetch_in_thread_sync(0); //fetch_in_thread(0);
    //printf("detect_in_thread_sync\n");
    detect_in_thread_sync(0); //fetch_in_thread(0);
    det_img = in_img;
    det_s = in_s;
    det_s_2 = in_s_2;
    det_s_3 = in_s_3;

    //detect_in_thread_sync(0); //fetch_in_thread(0);

    //printf("Start free detections\n");
    for (j = 0; j < avg_frames / 2; ++j) {
        free_detections(dets, nboxes);
        if (DMR_execution || TMR_execution)
        {
            free_detections(dets_2, nboxes_2);
            if (TMR_execution) free_detections(dets_3, nboxes_3);
        }
        fetch_in_thread_sync(0); //fetch_in_thread(0);
        detect_in_thread_sync(0); //fetch_in_thread(0);
        det_img = in_img;
        det_s = in_s;
        det_s_2 = in_s_2;
        det_s_3 = in_s_3;
    }

    int count = 0;
    /*
    if(!prefix && !dont_show){
        int full_screen = 0;
        create_window_cv("Demo", full_screen, 1352, 1013);
    }
    */

    /*
    write_cv* output_video_writer = NULL;
    if (out_filename && !flag_exit)
    {
        int src_fps = 25;
        src_fps = get_stream_fps_cpp_cv(cap);
        output_video_writer =
            create_video_writer(out_filename, 'D', 'I', 'V', 'X', src_fps, get_width_mat(det_img), get_height_mat(det_img), 1);

        //'H', '2', '6', '4'
        //'D', 'I', 'V', 'X'
        //'M', 'J', 'P', 'G'
        //'M', 'P', '4', 'V'
        //'M', 'P', '4', '2'
        //'X', 'V', 'I', 'D'
        //'W', 'M', 'V', '2'
    }
    */

    int send_http_post_once = 0;
    const double start_time_lim = get_time_point();
    double before = get_time_point();
    double start_time = get_time_point();
    float avg_fps = 0;
    int frame_counter = 0;
    int global_frame_counter = 0;

//    while(1){
    while (1) {
        ++count;
        {
            //printf("Count: %d\n", count);

            if (FAULT_INJECTION && !SIMULTANEOUS_FAULTS)
            {
                int faulty_comp = 0;
                if (DMR_execution) faulty_comp = count % 2;
                if (TMR_execution) faulty_comp = count % 3;
                FAULTY_COMPONENT = faulty_comp;
                //printf("\nFAULTY_COMPONENT = %d\n", FAULTY_COMPONENT);
            }

            //const float nms = .45;    // 0.4F
            const float nms = .5;
            int local_nboxes = nboxes;
            int local_nboxes_2 = nboxes_2;
            int local_nboxes_3 = nboxes_3;
            detection *local_dets = dets;
            detection *local_dets_2 = dets_2;
            detection *local_dets_3 = dets_3;
            this_thread_yield();

            if (!benchmark) custom_atomic_store_int(&run_fetch_in_thread, 1); // if (custom_create_thread(&fetch_thread, 0, fetch_in_thread, 0)) error("Thread creation failed", DARKNET_LOC);
            custom_atomic_store_int(&run_detect_in_thread, 1); // if (custom_create_thread(&detect_thread, 0, detect_in_thread, 0)) error("Thread creation failed", DARKNET_LOC);

            //if (nms) do_nms_obj(local_dets, local_nboxes, l.classes, nms);    // bad results
            /*
            if (nms) {
                if (l.nms_kind == DEFAULT_NMS) do_nms_sort(local_dets, local_nboxes, l.classes, nms);
                else diounms_sort(local_dets, local_nboxes, l.classes, nms, l.nms_kind, l.beta_nms);
            }
            */

            //printf("Start nms\n");
            if (nms)
            {
                do_nms_sort(local_dets, local_nboxes, l.classes, nms);

                if (DMR_execution || TMR_execution)
                {
                    do_nms_sort(local_dets_2, local_nboxes_2, l.classes, nms);

                    if (TMR_execution) do_nms_sort(local_dets_3, local_nboxes_3, l.classes, nms);
                }
            }

            if (l.embedding_size) set_track_id(local_dets, local_nboxes, demo_thresh, l.sim_thresh, l.track_ciou_norm, l.track_history_size, l.dets_for_track, l.dets_for_show);

            //printf("\033[2J");
            //printf("\033[1;1H");
            //printf("\nFPS:%.1f\n", fps);
            printf("Objects:\n\n");

            ++frame_id;
            /*
            if (demo_json_port > 0) {
                int timeout = 400000;
                send_json(local_dets, local_nboxes, l.classes, demo_names, frame_id, demo_json_port, timeout);
            }

            //char *http_post_server = "webhook.site/898bbd9b-0ddd-49cf-b81d-1f56be98d870";
            if (http_post_host && !send_http_post_once) {
                int timeout = 3;            // 3 seconds
                int http_post_port = 80;    // 443 https, 80 http
                if (send_http_post_request(http_post_host, http_post_port, filename,
                    local_dets, nboxes, classes, names, frame_id, ext_output, timeout))
                {
                    if (time_limit_sec > 0) send_http_post_once = 1;
                }
            }
            */

            //if (!benchmark && !dontdraw_bbox) draw_detections_cv_v3(show_img, local_dets, local_nboxes, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count);
            //free_detections(local_dets, local_nboxes);

            char *save_file_name = NULL;
            if (save_dets != NULL)
            {
                char buff[256];
                sprintf(buff, "frame_%d.txt", count);
                save_file_name = calloc((strlen(save_dets) + strlen(buff) + 1), sizeof(char));
                //char *token = strtok(ent->d_name, ".");
                strcpy(save_file_name, save_dets);
                strcat(save_file_name, buff);
                //printf("Video save file: %s\n", save_file_name);
            }

            //printf("After save dets\n");

            if (!DMR_execution && !TMR_execution)
            {
                draw_detections_cv_v3(show_img, local_dets, local_nboxes, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                char buff[512];
                sprintf(buff, "frame_%d.jpg", count);
                char buff_2[512];
                if (save_dets != NULL)
                {
                    strcpy(buff_2, save_dets);
                    strcat(buff_2, buff);
                    //printf("buff_2: %s\n", buff_2);
                }
                if (count > 1 && save_dets != NULL) save_cv_jpg(show_img, buff_2);
            }

            //printf("After draw_dets if not DMR / TMR\n");

            mat_cv* show_img_clone_1 = clone_mat(show_img);
            mat_cv* show_img_clone_2 = clone_mat(show_img);
            mat_cv* show_img_merging = clone_mat(show_img);

            // FOR PRINTING THE IMAGES OF EACH OUTPUT
            if (DMR_execution || TMR_execution)
            {
                printf("First Input\n");
                draw_detections_cv_v3(show_img, local_dets, local_nboxes, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);

                char buff[512];
                sprintf(buff, "frame_%d.jpg", count);
                char buff_2[512];
                if (save_dets != NULL)
                {
                    strcpy(buff_2, save_dets);
                    strcat(buff_2, buff);
                    //printf("buff_2: %s\n", buff_2);
                }
                if (count > 1 && save_dets != NULL) save_cv_jpg(show_img, buff_2);

                printf("\nSecond Input\n");
                draw_detections_cv_v3(show_img_clone_1, local_dets_2, local_nboxes_2, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                char buff_3[512];
                sprintf(buff_3, "frame_%d_2.jpg", count);
                char buff_4[512];
                if (save_dets != NULL)
                {
                    strcpy(buff_4, save_dets);
                    strcat(buff_4, buff_3);
                    //printf("buff_4: %s\n", buff_4);
                }
                if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_clone_1, buff_4);

                if (TMR_execution)
                {
                    printf("\nThird Input\n");
                    draw_detections_cv_v3(show_img_clone_2, local_dets_3, local_nboxes_3, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                    char buff_5[512];
                    sprintf(buff_5, "frame_%d_3.jpg", count);
                        char buff_6[512];
                    if (save_dets != NULL)
                    {
                        strcpy(buff_6, save_dets);
                        strcat(buff_6, buff_5);
                        //printf("buff_6: %s\n", buff_6);
                    }
                    if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_clone_2, buff_6);
                }
            }

            if (MAX_merging)
            {
                detection *dets_1_2_fused = NULL;
                detection *dets_1_2_3_fused = NULL;

                int nboxes_1_2 = 0;
                int nboxes_1_2_3 = 0;

                if (DMR_execution || TMR_execution)
                {
                    nboxes_1_2 = local_nboxes + local_nboxes_2;
                    dets_1_2_fused = (detection*)calloc(nboxes_1_2, sizeof(detection));
                    fuse_detections(local_dets, local_dets_2, dets_1_2_fused, local_nboxes, local_nboxes_2);

                    if (DMR_execution)
                    {
                        //printf("Doing MAXIMUM merging DMR\n");

                        do_nms_sort(dets_1_2_fused, nboxes_1_2, l.classes, nms);

                        printf("\nMAX_merging\n");
                        draw_detections_cv_v3(show_img_merging, dets_1_2_fused, nboxes_1_2, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                        char buff[512];
                        sprintf(buff, "frame_%d_DMR_MAX.jpg", count);
                        char buff_2[512];
                                                if (save_dets != NULL)
                        {
                            strcpy(buff_2, save_dets);
                            strcat(buff_2, buff);
                            //printf("buff_2: %s\n", buff_2);
                        }
                        if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_merging, buff_2);
                    }
                    else
                    {
                        //printf("Doing MAXIMUM merging TMR\n");
                        nboxes_1_2_3 = nboxes_1_2 + local_nboxes_3;
                        dets_1_2_3_fused = (detection*)calloc(nboxes_1_2_3, sizeof(detection));
                        fuse_detections(dets_1_2_fused, local_dets_3, dets_1_2_3_fused, nboxes_1_2, local_nboxes_3);

                        do_nms_sort(dets_1_2_3_fused, nboxes_1_2_3, l.classes, nms);

                        printf("\nMAX_merging\n");
                        draw_detections_cv_v3(show_img_merging, dets_1_2_3_fused, nboxes_1_2_3, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                        char buff[512];
                        sprintf(buff, "frame_%d_TMR_MAX.jpg", count);
                        char buff_2[512];
                        if (save_dets != NULL)
                        {
                            strcpy(buff_2, save_dets);
                            strcat(buff_2, buff);
                            //printf("buff_2: %s\n", buff_2);
                        }
                        if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_merging, buff_2);
                    }
                    if (DMR_execution && dets_1_2_fused != NULL) free_detections(dets_1_2_fused, nboxes_1_2);
                    if (TMR_execution && dets_1_2_3_fused != NULL) free_detections(dets_1_2_3_fused, nboxes_1_2_3);
                }
            }
            else if (AVG_merging)
            {
                if (DMR_execution)
                {
                    avg_outputs(&net, 2);
                }
                else if (TMR_execution)
                {
                    avg_outputs(&net, 3);
                }

                int nboxes_avg = 0;
                detection *dets_avg = get_network_boxes(&net, get_width_mat(show_img_merging), get_height_mat(show_img_merging), demo_thresh, demo_thresh, 0, 1, &nboxes_avg, 0);

                calculate_best_class_idx(dets_avg, nboxes_avg);

                if (KITTI_dataset)
                {
                    fuse_vehicle_classes_KITTI(dets_avg, nboxes_avg, num_classes);
                    ignore_not_vehicle(dets_avg, nboxes_avg, num_classes);
                }
                else
                {
                    fuse_vehicle_classes(dets_avg, nboxes_avg, num_classes);
                    ignore_not_vehicle_person(dets_avg, nboxes_avg, num_classes);
                }

                if (FAULT_INJECTION) fix_zero_area_faults(dets_avg, nboxes_avg, num_classes);

                if (nms) do_nms_sort(dets_avg, nboxes_avg, l.classes, nms);

                printf("\nAVG_merging\n");
                draw_detections_cv_v3(show_img_merging, dets_avg, nboxes_avg, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                char buff[512];
                sprintf(buff, "frame_%d_AVG.jpg", count);
                char buff_2[512];
                if (save_dets != NULL)
                {
                    strcpy(buff_2, save_dets);
                    strcat(buff_2, buff);
                    //printf("buff_2: %s\n", buff_2);
                }
                if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_merging, buff_2);

		free_detections(dets_avg, nboxes_avg);
            }
            else if (VOT_merging)
            {
                detection *dets_1_2_vote = (detection*)xcalloc(local_nboxes, sizeof(detection)); // Voting will have at most "nboxes" detections
                detection *dets_1_3_vote = (detection*)xcalloc(local_nboxes, sizeof(detection)); // Voting will have at most "nboxes" detections
                detection *dets_2_3_vote = (detection*)xcalloc(local_nboxes_2, sizeof(detection)); // Voting will have at most "nboxes_2" detections

                int nboxes_1_2_vote = 0;
                int nboxes_1_3_vote = 0;
                int nboxes_2_3_vote = 0;

                vote_detections(local_dets, local_dets_2, dets_1_2_vote, local_nboxes, local_nboxes_2, &nboxes_1_2_vote, iou_thresh);
                vote_detections(local_dets, local_dets_3, dets_1_3_vote, local_nboxes, local_nboxes_3, &nboxes_1_3_vote, iou_thresh);
                vote_detections(local_dets_2, local_dets_3, dets_2_3_vote, local_nboxes_2, local_nboxes_3, &nboxes_2_3_vote, iou_thresh);

                int nboxes_voting = nboxes_1_2_vote + nboxes_1_3_vote + nboxes_2_3_vote;

                detection *voting_fused = (detection*)xcalloc(nboxes_1_2_vote + nboxes_1_3_vote, sizeof(detection));
                fuse_detections(dets_1_2_vote, dets_1_3_vote, voting_fused, nboxes_1_2_vote, nboxes_1_3_vote);

                detection *voting_fused_2 = (detection*)xcalloc(nboxes_voting, sizeof(detection));
                fuse_detections(voting_fused, dets_2_3_vote, voting_fused_2, nboxes_1_2_vote + nboxes_1_3_vote, nboxes_2_3_vote);

                if (nms) do_nms_sort(voting_fused_2, nboxes_voting, l.classes, nms);

                printf("\nVOT_merging\n");
                draw_detections_cv_v3(show_img_merging, voting_fused_2, nboxes_voting, demo_thresh, demo_names, demo_alphabet, demo_classes, demo_ext_output, count, save_file_name);
                char buff[512];
                sprintf(buff, "frame_%d_VOT.jpg", count);
                char buff_2[512];
                if (save_dets != NULL)
                {
                    strcpy(buff_2, save_dets);
                    strcat(buff_2, buff);
                    //printf("buff_2: %s\n", buff_2);
                }
                if (count > 1 && save_dets != NULL) save_cv_jpg(show_img_merging, buff_2);

		if (dets_1_2_vote != NULL) free_detections(dets_1_2_vote, local_nboxes);
                if (dets_1_3_vote != NULL) free_detections(dets_1_3_vote, local_nboxes);
                if (dets_2_3_vote != NULL) free_detections(dets_2_3_vote, local_nboxes_2);

                if (voting_fused != NULL) free_detections(voting_fused, nboxes_1_2_vote + nboxes_1_3_vote);
                if (voting_fused_2 != NULL) free_detections(voting_fused_2, nboxes_voting);
            }

            printf("After Mergings\n");

            printf("\nFPS:%.1f \t AVG_FPS:%.1f\n", fps, avg_fps);

            /*
            if(!prefix){
                if (!dont_show) {
                    const int each_frame = max_val_cmp(1, avg_fps / 60);
                    if(global_frame_counter % each_frame == 0) show_image_mat(show_img, "Demo");
                    int c = wait_key_cv(1);
                    if (c == 10) {
                        if (frame_skip == 0) frame_skip = 60;
                        else if (frame_skip == 4) frame_skip = 0;
                        else if (frame_skip == 60) frame_skip = 4;
                        else frame_skip = 0;
                    }
                    else if (c == 27 || c == 1048603) // ESC - exit (OpenCV 2.x / 3.x)
                    {
                        flag_exit = 1;
                    }
                }
            }else{
                char buff[256];
                sprintf(buff, "%s_%08d.jpg", prefix, count);
                if(show_img) save_cv_jpg(show_img, buff);
            }
            */

            // if you run it with param -mjpeg_port 8090  then open URL in your web-browser: http://localhost:8090
            /*
            if (mjpeg_port > 0 && show_img) {
                int port = mjpeg_port;
                int timeout = 400000;
                int jpeg_quality = 40;    // 1 - 100
                send_mjpeg(show_img, port, timeout, jpeg_quality);
            }

            // save video file
            if (output_video_writer && show_img) {
                write_frame_cv(output_video_writer, show_img);
                printf("\n cvWriteFrame \n");
            }
            */

            while (custom_atomic_load_int(&run_detect_in_thread)) {
                if(avg_fps > 180) this_thread_yield();
                else this_thread_sleep_for(thread_wait_ms);   // custom_join(detect_thread, 0);
            }
            if (!benchmark) {
                while (custom_atomic_load_int(&run_fetch_in_thread)) {
                    if (avg_fps > 180) this_thread_yield();
                    else this_thread_sleep_for(thread_wait_ms);   // custom_join(fetch_thread, 0);
                }
                //printf("free det_s\n");
                free_image(det_s);
                if (DMR_execution || TMR_execution)
                {
                    //printf("free det_s_2\n");
                    free_image(det_s_2);
                    //printf("done free det_s_2\n");
                    if (TMR_execution) free_image(det_s_3);
                }
            }

            if (time_limit_sec > 0 && (get_time_point() - start_time_lim)/1000000 > time_limit_sec) {
                printf(" start_time_lim = %f, get_time_point() = %f, time spent = %f \n", start_time_lim, get_time_point(), get_time_point() - start_time_lim);
                break;
            }

            if (flag_exit == 1) break;

            if(delay == 0){
                if(!benchmark) release_mat(&show_img);
                show_img = det_img;
            }
            det_img = in_img;
            det_s = in_s;
            det_s_2 = in_s_2;
            det_s_3 = in_s_3;

            //printf("free local_dets\n");
            free_detections(local_dets, local_nboxes);

            // CHANGES FOR DMR TMR INFERENCE
            if (DMR_execution || TMR_execution)
            {
                free_detections(local_dets_2, local_nboxes_2);

                if (TMR_execution) free_detections(local_dets_3, local_nboxes_3);
            }
        }
        --delay;
        if(delay < 0){
            delay = frame_skip;

            //double after = get_wall_time();
            //float curr = 1./(after - before);
            double after = get_time_point();    // more accurate time measurements
            float curr = 1000000. / (after - before);
            fps = fps*0.9 + curr*0.1;
            before = after;

            float spent_time = (get_time_point() - start_time) / 1000000;
            frame_counter++;
            global_frame_counter++;
            if (spent_time >= 3.0f) {
                //printf(" spent_time = %f \n", spent_time);
                avg_fps = frame_counter / spent_time;
                frame_counter = 0;
                start_time = get_time_point();
            }
        }
    }
    printf("input video stream closed. \n");
    /*
    if (output_video_writer) {
        release_video_writer(&output_video_writer);
        printf("output_video_writer closed. \n");
    }
    */

    //printf("Starting sleep\n");
    this_thread_sleep_for(thread_wait_ms);
    //printf("End sleep\n");

    custom_join(detect_thread, 0);
    //printf("Join detect\n");
    custom_join(fetch_thread, 0);
    //printf("Join thread\n");

    // free memory
    free_image(in_s);
    free_detections(dets, nboxes);

    if (DMR_execution || TMR_execution)
    {
        free_image(in_s_2);
        free_detections(dets_2, nboxes_2);
        if (TMR_execution)
        {
            free_image(in_s_3);
            free_detections(dets_3, nboxes_3);
        }
    }

    demo_index = (avg_frames + demo_index - 1) % avg_frames;
    for (j = 0; j < avg_frames; ++j) {
            release_mat(&cv_images[j]);
    }
    free(cv_images);

    free_ptrs((void **)names, net.layers[net.n - 1].classes);

    const int nsize = 8;
    for (j = 0; j < nsize; ++j) {
        for (i = 32; i < 127; ++i) {
            free_image(alphabet[j][i]);
        }
        free(alphabet[j]);
    }
    free(alphabet);
    free_network(net);
    //cudaProfilerStop();
}
#else
//void demo(char *cfgfile, char *weightfile, float thresh, float hier_thresh, int cam_index, const char *filename, char **names, int classes, int avgframes,
//    int frame_skip, char *prefix, char *out_filename, int mjpeg_port, int dontdraw_bbox, int json_port, int dont_show, int ext_output, int letter_box_in, int time_limit_sec, char *http_post_host,
//    int benchmark, int benchmark_layers)
void demo(char *cfgfile, char *weightfile, float thresh, float hier_thresh, int cam_index, const char *filename, char **names, int classes, int avgframes,
    int frame_skip, char *prefix, char *out_filename, int mjpeg_port, int dontdraw_bbox, int json_port, int dont_show, int ext_output, int letter_box_in, int time_limit_sec, char *http_post_host, int benchmark, int benchmark_layers, int *img_transformations, char *save_dets)
{
    fprintf(stderr, "Demo needs OpenCV for webcam images.\n");
}
#endif
