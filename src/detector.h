#ifndef DETECTOR_H
#define DETECTOR_H

#include "image.h"
#ifdef __cplusplus
extern "C" {
#endif

    void undo_transformation(network *net, int num_output, int img_transformation, image im);
    void avg_outputs(network *net, int n_outputs);
    void vote_detections(detection *dets, detection *dets_2, detection *dets_1_2_vote, int nboxes, int nboxes_2, int *nboxes_1_2_vote, float iou_thresh);
    void fuse_detections(detection *dets, detection *dets_2, detection *dets_1_2_fused, int nboxes, int nboxes_2);
    void calculate_best_class_idx(detection *dets, int nboxes);
    void fuse_vehicle_classes(detection *dets, int nboxes, int num_classes);
    void fuse_vehicle_classes_KITTI(detection *dets, int nboxes, int num_classes);
    void ignore_not_vehicle_person(detection *dets, int nboxes, int num_classes);
    void ignore_not_vehicle(detection *dets, int nboxes, int num_classes);
        void fix_zero_area_faults(detection *dets, int nboxes, int num_classes);
#ifdef __cplusplus
}
#endif

#endif
