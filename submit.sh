#!/bin/bash
#SBATCH --job-name=YOLOv4
#SBATCH -D .
#SBATCH --output=./YOLOv4_%j.out
#SBATCH --error=./YOLOv4_%j.err
#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=8
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

./darknet detector demo cfg/coco.data cfg/yolov4.cfg yolov4.weights video.avi -out_filename result.avi -avgframes 3 -dont_show
